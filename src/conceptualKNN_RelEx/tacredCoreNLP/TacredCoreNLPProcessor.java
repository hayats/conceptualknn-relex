package conceptualKNN_RelEx.tacredCoreNLP;

import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.pipeline.CoreDocument;
import edu.stanford.nlp.pipeline.CoreSentence;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.semgraph.SemanticGraph;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentContainer;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

public class TacredCoreNLPProcessor {
	public static final String DESCRIPTION = "Reprocess TACRED dataset JSON files for using it in C-KNN-RelEx";

	public static void addCliArgs(ArgumentContainer parser) {
		parser.description(DESCRIPTION);

		parser.addArgument("inputDir")
				.type(Arguments.fileType().verifyIsDirectory())
				.help("Path to the TACRED dataset directory");

		parser.addArgument("outputDir")
				.type(Arguments.fileType().verifyIsDirectory())
				.help("Path to the output directory");
	}

	public static void main(String[] args) throws InterruptedException {
		ArgumentParser parser = ArgumentParsers.newFor(TacredCoreNLPProcessor.class.getName()).build();
		addCliArgs(parser);

		try {
			execute(parser.parseArgs(args));
		} catch(ArgumentParserException e) {
			parser.handleError(e);
			System.exit(1);
		}
	}

	public static void execute(Namespace namespece) throws InterruptedException {
		String inputDir = namespece.getString("inputDir");
		String outputDir = namespece.getString("outputDir");

		String[] fileNames = new String[3];
		fileNames[0] = "train.json";
		fileNames[1] = "dev.json";
		fileNames[2] = "test.json";

		TacredCoreNLPProcessor.TacredThread[] threads = new TacredThread[3];

		for(int i = 0; i < 3; i++) {
			String[] arg = new String[4];
			arg[0] = inputDir + "/" + fileNames[i] + ".json";
			arg[1] = outputDir + "/" + fileNames[i] + ".json";
			arg[2] = outputDir + "/" + fileNames[i] + "_err" + ".json";
			arg[3] = outputDir + "/" + fileNames[i] + "_split" + ".json";
			System.out.printf("Processing file %s [1/3]\n", fileNames[i]);
			threads[i] = new TacredThread(arg);
			threads[i].start();
		}

		for(int i = 0; i < 3; i++) {
			threads[i].join();
		}
	}

	public static String tokensToSentence(JSONArray tokens) {
		StringBuilder sentence = new StringBuilder();
		for(int i = 0; i < tokens.length(); i++) {
			if(!(i == 0)) {
				sentence.append(" ");
			}
			sentence.append(tokens.getString(i));
		}

		return sentence.toString();
	}

	public static void run(String[] args) throws IOException {
		String inputFile = args[0];
		String outputFile = args[1];
		String errorFile = args[2];
		String splitFile = args[3];
		JSONArray errors = new JSONArray();

		JSONArray unprocessedSentences = new JSONArray(new JSONTokener(new FileInputStream(inputFile)));
		JSONArray processedSentences = new JSONArray();
		int nbErr = 0;
		JSONArray splitSentences = new JSONArray();

		Properties props = new Properties();
		props.setProperty("annotators", "tokenize,ssplit,pos,lemma,ner,parse");
		StanfordCoreNLP pipeline = new StanfordCoreNLP(props);

		int size = unprocessedSentences.length();
		for(int i = 0; i < size; i++) {
			JSONObject sentence = unprocessedSentences.getJSONObject(i);
			System.out.printf("[%d/%d] Processing sentence %s\n", i + 1, size, sentence.getString("id"));
			try {
				processedSentences.put(processSentence(pipeline, sentence, splitSentences));
			} catch(ConversionException e) {
				e.printStackTrace();
				errors.put(sentence);
				nbErr++;
			}
		}

		System.out.printf("\nProcessing finished with %d errors, %d split sentences\n", nbErr, splitSentences.length());

		FileWriter writer = new FileWriter(outputFile);
		processedSentences.write(writer, 0, 4);
		writer.flush();
		writer.close();

		FileWriter errWriter = new FileWriter(errorFile);
		errors.write(errWriter, 0, 4);
		errWriter.flush();
		errWriter.close();

		FileWriter splitWriter = new FileWriter(splitFile);
		splitWriter.write(splitSentences.toString());
		splitSentences.write(splitWriter);
		splitWriter.flush();
		splitWriter.close();
	}

	private static JSONObject processSentence(StanfordCoreNLP pipeline, JSONObject unprocessedSentence, JSONArray splitSentences) throws ConversionException {
		JSONObject processedSentence = new JSONObject();

		processedSentence.put("id", unprocessedSentence.getString("id"));
		processedSentence.put("docid", unprocessedSentence.getString("docid"));
		processedSentence.put("relation", unprocessedSentence.getString("relation"));
		processedSentence.put("subj_type", unprocessedSentence.getString("subj_type"));
		processedSentence.put("obj_type", unprocessedSentence.getString("obj_type"));

		String sentence = TacredCoreNLPProcessor.tokensToSentence(unprocessedSentence.getJSONArray("token"));

		CoreDocument document = new CoreDocument(sentence);
		pipeline.annotate(document);

		if(document.sentences().size() > 1) {
			System.err.println("Warning: sentence split in several sentences");
			splitSentences.put(unprocessedSentence.getString("id"));
		}

		int offset = 0;
		for(CoreSentence coreSentence : document.sentences()) {
			SemanticGraph graph = coreSentence.dependencyParse();

			int i = 1;
			IndexedWord word = graph.getNodeByIndexSafe(i);
			while(word != null) {
				processedSentence.append("token", word.word());
				processedSentence.append("lemma", word.lemma());
				processedSentence.append("stanford_pos", word.tag());
				processedSentence.append("stanford_ner", (word.ner() != null ? word.ner() : "O"));

				IndexedWord parent = graph.getParent(word);
				if(parent == null) {
					processedSentence.append("stanford_head", 0);
					processedSentence.append("stanford_deprel", "ROOT");
				} else {
					if(parent.index() == 0) {
						processedSentence.append("stanford_head", 0);
					} else {
						processedSentence.append("stanford_head", parent.index() + offset);
					}
					processedSentence.append("stanford_deprel", graph.getEdge(parent, word).getRelation().toString());
				}

				i++;
				word = graph.getNodeByIndexSafe(i);
			}
			offset = processedSentence.getJSONArray("token").length();
		}

		int oldSubjStart = unprocessedSentence.getInt("subj_start");
		int oldSubjEnd = unprocessedSentence.getInt("subj_end");
		int oldObjStart = unprocessedSentence.getInt("obj_start");
		int oldObjEnd = unprocessedSentence.getInt("obj_end");

		JSONArray unprocTokens = unprocessedSentence.getJSONArray("token");
		JSONArray procTokens = processedSentence.getJSONArray("token");
		int procTokensSize = procTokens.length();

		int subjStart, objStart;
		if(oldSubjStart < procTokensSize) {
			subjStart = oldSubjStart;
		} else {
			subjStart = procTokensSize - 1;
		}
		if(oldObjStart < procTokensSize) {
			objStart = oldObjStart;
		} else {
			objStart = procTokensSize - 1;
		}
		int subjEnd = subjStart;
		int objEnd = objStart;

		StringBuilder subjBuilder = new StringBuilder();
		for(int j = oldSubjStart; j <= oldSubjEnd; j++) {
			subjBuilder.append(unprocTokens.getString(j));
		}
		String subject = subjBuilder.toString();
		String builtSubject;

		boolean subjectFound = false;
		boolean inbound = true;
		while(!subjectFound && inbound) {
			builtSubject = procTokens.getString(subjStart);
			if(subject.startsWith(builtSubject)) {
				int end = subjStart + 1;
				while(end < procTokensSize && subject.startsWith(builtSubject) && !subject.equals(builtSubject)) {
					builtSubject = builtSubject + procTokens.getString(end);
					end++;
				}
				if(subject.equals(builtSubject)) {
					subjectFound = true;
					subjEnd = end - 1;
				}
			}

			if(!subjectFound) {
				if(subjStart == oldSubjStart) {
					if(subjStart + 1 < procTokensSize) {
						subjStart++;
					} else if(subjStart - 1 >= 0) {
						subjStart--;
					} else {
						inbound = false;
					}
				} else if(subjStart > oldSubjStart) {
					if(oldSubjStart - (subjStart - oldSubjStart) >= 0) {
						subjStart = oldSubjStart - (subjStart - oldSubjStart);
					} else if(subjStart + 1 < procTokensSize) {
						subjStart++;
					} else {
						inbound = false;
					}
				} else { // subjStart < oldSubjStart
					if(oldSubjStart + (oldSubjStart - subjStart) + 1 < procTokensSize) {
						subjStart = oldSubjStart + (oldSubjStart - subjStart) + 1;
					} else if(subjStart - 1 >= 0) {
						subjStart--;
					} else {
						inbound = false;
					}
				}
			}
		}

		if(subjectFound) {
			processedSentence.put("subj_start", subjStart);
			processedSentence.put("subj_end", subjEnd);
		} else {
			throw new ConversionException("Subject not found");
		}

		//Identify object
		StringBuilder objBuilder = new StringBuilder();
		for(int j = oldObjStart; j <= oldObjEnd; j++) {
			objBuilder.append(unprocTokens.getString(j));
		}
		String object = objBuilder.toString();
		String builtObject;

		boolean objectFound = false;
		inbound = true;
		while(!objectFound && inbound) {
			builtObject = procTokens.getString(objStart);
			if(object.startsWith(builtObject)) {
				int end = objStart + 1;
				while(end < procTokensSize && object.startsWith(builtObject) && !object.equals(builtObject)) {
					builtObject = builtObject + procTokens.getString(end);
					end++;
				}
				if(object.equals(builtObject)) {
					objectFound = true;
					objEnd = end - 1;
				}
			}

			if(!objectFound) {
				if(objStart == oldObjStart) {
					if(objStart + 1 < procTokensSize) {
						objStart++;
					} else if(objStart - 1 >= 0) {
						objStart--;
					} else {
						inbound = false;
					}
				} else if(objStart > oldObjStart) {
					if(oldObjStart - (objStart - oldObjStart) >= 0) {
						objStart = oldObjStart - (objStart - oldObjStart);
					} else if(objStart + 1 < procTokensSize) {
						objStart++;
					} else {
						inbound = false;
					}
				} else { // objStart < oldObjStart
					if(oldObjStart + (oldObjStart - objStart) + 1 < procTokensSize) {
						objStart = oldObjStart + (oldObjStart - objStart) + 1;
					} else if(objStart - 1 >= 0) {
						objStart--;
					} else {
						inbound = false;
					}
				}
			}
		}

		if(objectFound) {
			processedSentence.put("obj_start", objStart);
			processedSentence.put("obj_end", objEnd);
		} else {
			throw new ConversionException("Object not found");
		}

		return processedSentence;
	}

	public static class TacredThread extends Thread {
		private final String[] args;

		public TacredThread(String[] args) {
			super();
			this.args = args;
		}

		public void run() {
			try {
				TacredCoreNLPProcessor.run(args);
			} catch(IOException e) {
				e.printStackTrace();
			}
		}
	}
}
