package conceptualKNN_RelEx.tacredCoreNLP;

public class ConversionException extends Exception {
	public ConversionException(String s) {
		super(s);
	}
}
