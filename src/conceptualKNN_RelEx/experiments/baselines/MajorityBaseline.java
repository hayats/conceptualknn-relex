package conceptualKNN_RelEx.experiments.baselines;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MajorityBaseline extends Baseline {
	private final Map<String, Integer> relations = new HashMap<>();
	private String majorityRelation = null;

	public MajorityBaseline(boolean verbose, boolean excludeNegative) {
		super(verbose, excludeNegative);
	}

	@Override
	public void importExample(JSONObject sentence) {
		String relation = sentence.getString("relation");

		if(!relation.equals("no_relation")) {
			if(relations.containsKey(relation)) {
				relations.put(relation, relations.get(relation) + 1);
			} else {
				relations.put(relation, 1);
			}
			if(majorityRelation == null || relations.get(majorityRelation) < relations.get(relation)) {
				this.majorityRelation = relation;
			}
		}
	}

	@Override
	public String makePrediction(JSONObject sentence) {
		return majorityRelation;
	}

	@Override
	public void printModel() {
		ArrayList<String> relationTab = new ArrayList<>(relations.keySet());
		relationTab.sort((a,b) -> relations.get(b) - relations.get(a));
		relationTab.forEach(r ->
				System.out.printf("%s\t%s\n", r, relations.get(r))
		);
		System.out.printf("Majority relation: %s\n", this.majorityRelation);
	}
}
