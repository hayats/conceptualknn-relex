package conceptualKNN_RelEx.experiments.baselines;

import org.json.JSONObject;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

public class RandomBaseline extends Baseline {
	private final Set<String> relations = new HashSet<>();
	private final Random random = new Random();

	public RandomBaseline(boolean verbose, boolean excludeNegative) {
		super(verbose, excludeNegative);
	}

	public void importExample(JSONObject sentence) {
		String relation = sentence.getString("relation");
		if(!relation.equals("no_relation") && !this.relations.contains(relation)) {
			relations.add(relation);
		}
	}

	@Override
	public String makePrediction(JSONObject sentence) {
		int randomNumber = random.nextInt(relations.size());
		Iterator<String> iter = relations.iterator();

		String predictedRelation = null;
		int index = 0;
		while(iter.hasNext()) {
			predictedRelation = iter.next();
			index++;
			if(index == randomNumber) {
				return predictedRelation;
			}
		}

		return predictedRelation;
	}

	@Override
	public void printModel() {
		System.out.printf("%d relations:\n", relations.size());
		this.relations.forEach(s ->
				System.out.printf("\t%s\n", s)
		);
	}
}
