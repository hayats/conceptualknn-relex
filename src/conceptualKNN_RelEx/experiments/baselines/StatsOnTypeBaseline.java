package conceptualKNN_RelEx.experiments.baselines;

import org.json.JSONObject;

import java.util.*;

public class StatsOnTypeBaseline extends Baseline {
	private boolean importNegativeInModel = false;
	private final Map<String, Map<String, Map<String, Integer>>> relationsByTypes = new HashMap<>();
	private final Map<String, Map<String, String>> predictionByTypes = new HashMap<>();
	private final Map<String, Integer> relationsCardinality = new HashMap<>();
	private final Map<String, Map<String, Integer>> typesCardinality = new HashMap<>();
	private final Set<String> types = new HashSet<>();

	public StatsOnTypeBaseline(boolean verbose, boolean excludeNegative) {
		super(verbose, excludeNegative);
	}

	public StatsOnTypeBaseline(boolean verbose, boolean excludeNegative, boolean negativeInModel) {
		this(verbose, excludeNegative);
		this.importNegativeInModel = negativeInModel;
	}

	@Override
	public void importExample(JSONObject sentence) {
		String relation = sentence.getString("relation");
		String subjType = sentence.getString("subj_type");
		String objType = sentence.getString("obj_type");

		if(importNegativeInModel || !relation.equals("no_relation")) {
			if(!relationsCardinality.containsKey(relation)) {
				relationsCardinality.put(relation, 1);
			} else {
				relationsCardinality.put(relation, relationsCardinality.get(relation) + 1);
			}

			types.add(subjType);
			types.add(objType);

			if(!this.relationsByTypes.containsKey(subjType)) {
				this.relationsByTypes.put(subjType, new HashMap<>());
				this.predictionByTypes.put(subjType, new HashMap<>());
				this.typesCardinality.put(subjType, new HashMap<>());
			}

			Map<String, Map<String, Integer>> subjMapRelation = this.relationsByTypes.get(subjType);
			Map<String, String> subjMapPrediction = this.predictionByTypes.get(subjType);
			Map<String, Integer> objTypesCardinality = this.typesCardinality.get(subjType);
			if(!subjMapRelation.containsKey(objType)) {
				subjMapRelation.put(objType, new HashMap<>());
				subjMapPrediction.put(objType, null);
				objTypesCardinality.put(objType, 1);
			} else {
				objTypesCardinality.put(objType, objTypesCardinality.get(objType) + 1);
			}

			Map<String, Integer> relations = subjMapRelation.get(objType);
			if(relations.containsKey(relation)) {
				relations.put(relation, relations.get(relation) + 1);
			} else {
				relations.put(relation, 1);
			}
			if(subjMapPrediction.get(objType) == null || relations.get(relation) > relations.get(subjMapPrediction.get(objType))) {
				subjMapPrediction.put(objType, relation);
			}
		}
	}

	@Override
	public String makePrediction(JSONObject sentence) {
		String subjType = sentence.getString("subj_type");
		String objType = sentence.getString("obj_type");

		if(predictionByTypes.containsKey(subjType) && predictionByTypes.get(subjType).containsKey(objType) && predictionByTypes.get(subjType).get(objType) != null) {
			return predictionByTypes.get(subjType).get(objType);
		} else {
			return "no_relation";
		}
	}

	@Override
	public void printModel() {
		List<String> relationTab = new ArrayList<>(relationsCardinality.keySet());
		relationTab.sort((a, b) -> relationsCardinality.get(b) - relationsCardinality.get(a));

		List<String> typesTab = new ArrayList<>(types);
		typesTab.sort(Comparator.naturalOrder());

		StringBuilder firstLine = new StringBuilder("SubjType\tObjType");
		for(String r : relationTab) {
			firstLine.append("\t").append(r);
		}
		System.out.println(firstLine.toString());

		for(String subj : typesTab) {
			for(String obj : typesTab) {
				if(this.relationsByTypes.containsKey(subj) && this.relationsByTypes.get(subj).containsKey(obj)) {
					StringBuilder line = new StringBuilder(String.format("%s\t%s", subj, obj));

					for(String rel : relationTab) {
						if(this.relationsByTypes.get(subj).get(obj).containsKey(rel)) {
							line.append("\t").append(this.relationsByTypes.get(subj).get(obj).get(rel));
						} else {
							line.append("\t").append(0);
						}
					}
					System.out.println(line);
				}
			}
		}

		StringBuilder lastLine = new StringBuilder("Total\tTotal");
		for(String r : relationTab) {
			lastLine.append("\t").append(this.relationsCardinality.get(r));
		}
		System.out.println(lastLine.toString());
	}

}
