package conceptualKNN_RelEx.experiments;

import conceptualKNN_RelEx.experiments.baselines.Baseline;
import conceptualKNN_RelEx.experiments.baselines.StatsOnTypeBaseline;
import conceptualKNN_RelEx.utils.AnalyzeResults;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentContainer;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import org.json.JSONArray;
import org.json.JSONTokener;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class BaselineRunner {
	public static final String DESCRIPTION = "Run the baseline method on the dataset";

	public static void addCliArgs(ArgumentContainer parser) {
		parser.description(DESCRIPTION);

		parser.addArgument("inputDir")
				.type(Arguments.fileType().verifyIsDirectory())
				.help("Path to the dataset directory");

		parser.addArgument("outputDir")
				.type(Arguments.fileType().verifyIsDirectory())
				.help("Path to the output directory");

		parser.addArgument("-v", "--verbose")
				.action(Arguments.storeTrue())
				.dest("verbose")
				.help("Trigger the display of the model used by the baseline");
	}

	public static void main(String[] args) throws IOException {
		ArgumentParser parser = ArgumentParsers.newFor(BaselineRunner.class.getName()).build();
		addCliArgs(parser);

		try {
			execute(parser.parseArgs(args));
		} catch(ArgumentParserException e) {
			parser.handleError(e);
		}
	}

	public static void execute(Namespace namespace) throws IOException {
		String inputDir = namespace.getString("inputDir");
		String outputDir = namespace.getString("outputDir");
		boolean verbose = namespace.getBoolean("verbose");
		String trainingPath = inputDir + "/train.json";
		String devPath = inputDir + "/dev.json";
		String testPath = inputDir + "/test.json";
		String goldPath = outputDir + "/gold.txt";
		String predPath = outputDir + "/prediction.txt";

		JSONArray trainingDataset = new JSONArray(new JSONTokener(new FileInputStream(trainingPath)));
		JSONArray devDataset = new JSONArray(new JSONTokener(new FileInputStream(devPath)));
		JSONArray testDataset = new JSONArray(new JSONTokener(new FileInputStream(testPath)));
		FileWriter guessWriter = new FileWriter(goldPath);
		FileWriter goldWriter = new FileWriter(predPath);

		Baseline model = new StatsOnTypeBaseline(verbose, true, false);
		model.importDataset(trainingDataset);
		model.importDataset(devDataset);

		model.generatePredictions(testDataset);

		List<String> gold = model.getGold();
		List<String> guess = model.getGuess();
		Map<String, Integer> goldCount = new HashMap<>();
		Map<String, Integer> guessCount = new HashMap<>();
		Map<String, Integer> correctCount = new HashMap<>();
		int wellPredicted = 0;
		for(int i = 0; i < gold.size(); i++) {
			goldWriter.write(gold.get(i) + "\n");
			guessWriter.write(guess.get(i) + "\n");
			goldCount.put(gold.get(i), goldCount.getOrDefault(gold.get(i), 0) + 1);
			guessCount.put(guess.get(i), guessCount.getOrDefault(guess.get(i), 0) + 1);
			if(gold.get(i).equals(guess.get(i))) {
				wellPredicted++;
				correctCount.put(gold.get(i), correctCount.getOrDefault(gold.get(i), 0) + 1);
			}
		}
		goldWriter.close();
		guessWriter.close();

		Set<String> relations = new HashSet<>();
		relations.addAll(goldCount.keySet());
		relations.addAll(guessCount.keySet());

		System.out.println("\nPer-relation analysis:");
		for(String rel :
				relations) {
			Map<String, Double> stats = AnalyzeResults.stats(goldCount.getOrDefault(rel, 0), correctCount.getOrDefault(rel, 0), guessCount.getOrDefault(rel, 0));
			System.out.printf("\t%s:\tP = %.3f\tR = %.3f\tF1 = %.3f\n", rel, stats.get("precision")*100, stats.get("recall")*100, stats.get("f1")*100);
		}
		System.out.println();

		float accuracy = (((float) wellPredicted) / gold.size()) * 100;
		System.out.printf("Nbr of examples: %d\n", gold.size());
		System.out.printf("Nbr of well predicted examples: %d\n", wellPredicted);
		System.out.printf("Accuracy: %.3f\n", accuracy);

		if(verbose) {
			System.out.println();
			model.printModel();
		}
	}
}
