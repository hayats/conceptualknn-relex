package conceptualKNN_RelEx.experiments;

import conceptualKNN_RelEx.RelationExtractionModel;
import conceptualKNN_RelEx.utils.AnalyzeResults;
import conceptualKNN_RelEx.utils.RelExException;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentContainer;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Experiment {

	public final static String DESCRIPTION = "Run a full ConceptualKNN experiment";

	public static void addCliArgs(ArgumentContainer parser) {
		parser.description(DESCRIPTION);

		parser.addArgument("datasetPath")
				.type(Arguments.fileType().verifyIsDirectory())
				.help("Path to the dataset directory");

		parser.addArgument("wordnetPath")
				.type(Arguments.fileType().verifyIsDirectory())
				.help("Path to WordNet dictionary");

		parser.addArgument("outputPath")
				.type(Arguments.fileType().verifyIsDirectory())
				.help("Path to the output directory");

		parser.addArgument("-v")
				.action(Arguments.storeTrue())
				.dest("verbose")
				.help("Verbose flag (disabled by default)");

		parser.addArgument("--skip-analysis")
				.action(Arguments.storeTrue())
				.dest("skipAnalysis")
				.help("Do not analyze results after processing");

		parser.addArgument("--no-pruning")
				.action(Arguments.storeFalse())
				.dest("pruned")
				.help("Disable the pruning strategy (enabled by default");

		parser.addArgument("--saturate-intensions")
				.action(Arguments.storeTrue())
				.dest("saturateIntensions")
				.help("Enable the saturation of the intensions");

		parser.addArgument("--luke-annotated")
				.action(Arguments.storeTrue())
				.dest("lukeAnnotation")
				.help("Enable the use of the LUKE annotation to discriminate positive examples from negative ones (disabled by default)");

		parser.addArgument("-n")
				.metavar("N")
				.type(Integer.class)
				.dest("nbThreads")
				.setDefault(1)
				.help("Number of threads (one by default)");

		parser.addArgument("--dotFilesDir")
				.metavar("dir")
				.type(Arguments.fileType().verifyIsDirectory())
				.dest("dotFilesDir")
				.setDefault("")
				.help("Path to the directory where to save DOT files");

		parser.addArgument("-t", "--timeout")
				.metavar("T")
				.type(Integer.class)
				.dest("timeout")
				.setDefault(10)
				.help("Timeout (in seconds) for the computation of each set of concept of neighbours (10 seconds by default)");
	}

	public static void main(String[] args) throws RelExException, IOException, InterruptedException {
		ArgumentParser parser = ArgumentParsers.newFor(Experiment.class.getName()).build();
		addCliArgs(parser);

		try {
			Namespace argNamespace = parser.parseArgs(args);
			Experiment experiment = new Experiment(argNamespace);
			experiment.execute();
		} catch(ArgumentParserException e) {
			parser.handleError(e);
			System.exit(1);
		}
	}

	private final RelationExtractionModel model;
	private int nbTestExamples;
	private int toBeProcessedTestExampleId;

	private final List<String> trainingFiles;
	private final List<String> testFiles;
	private final boolean verbose;
	private final boolean useLukeAnnotations;
	private final String outputDirectory;
	private final int nbThreads;
	private final boolean skipAnalysis;
	private final boolean saturateIntensions;

	public Experiment(Namespace argNamespace) throws RelExException, IOException {
		String datasetPath = argNamespace.getString("datasetPath");

		this.trainingFiles = new ArrayList<>();
		this.trainingFiles.add(datasetPath + "/train.json");
		this.trainingFiles.add(datasetPath + "/dev.json");

		this.testFiles = new ArrayList<>();
		this.testFiles.add(datasetPath + "/test.json");

		this.verbose = argNamespace.getBoolean("verbose");
		this.skipAnalysis = argNamespace.getBoolean("skipAnalysis");
		this.saturateIntensions = argNamespace.getBoolean("saturateIntensions");
		boolean pruneDependencyTree = argNamespace.getBoolean("pruned");

		this.useLukeAnnotations = argNamespace.getBoolean("lukeAnnotation");

		int maxComputationTime = argNamespace.getInt("timeout");

		String wordnetDictionaryPath = argNamespace.getString("wordnetPath");

		this.outputDirectory = argNamespace.getString("outputPath");

		this.nbThreads = argNamespace.getInt("nbThreads");

		List<String> scorers = new ArrayList<>();
		scorers.add("maximum_confidence");
		scorers.add("exponential_vote");
		scorers.add("maximum_confidence_bis");

		System.out.println("Model initialization...");
		this.model = new RelationExtractionModel(
				pruneDependencyTree,
				verbose,
				useLukeAnnotations,
				maxComputationTime,
				wordnetDictionaryPath,
				scorers,
				this.saturateIntensions
		);

		if(!argNamespace.getString("dotFilesDir").equals("")) {
			this.model.setDotFilesDir(argNamespace.getString("dotFilesDir"));
		}
	}

	public void execute() throws RelExException, IOException, InterruptedException {
		System.out.println("\nImporting training databases...");
		int i = 1;
		for(String file : trainingFiles) {
			System.out.printf("\n[%d/%d] Importing training database %s\n", i, trainingFiles.size(), file);
			this.model.importDatabase(new JSONArray(new JSONTokener(new FileInputStream(file))), true);
			i++;
		}

		System.out.println("\nImporting test databases...");
		i = 1;
		for(String file : testFiles) {
			System.out.printf("\n[%d/%d] Importing test database %s\n", i, testFiles.size(), file);
			this.model.importDatabase(new JSONArray(new JSONTokener(new FileInputStream(file))), false);
			i++;
		}

		System.out.println("\nClosing import phase...");
		this.model.setImportFinished();

		this.nbTestExamples = this.model.getTestPairList().size();
		this.toBeProcessedTestExampleId = 0;

		System.out.println("\nCreating threads...");
		List<ExperimentThread> threads = new ArrayList<>();

		for(int j = 0; j < nbThreads; j++) {
			ExperimentThread thread = new ExperimentThread(this, outputDirectory, verbose);
			threads.add(thread);
		}

		System.out.println("\nLaunching threads...");
		threads.forEach(Thread::start);

		for(ExperimentThread thread : threads) {
			thread.join();
		}

		if(this.useLukeAnnotations) {
			System.out.println("\nProcessing no_relation-annotated examples");
			for(String id : this.model.getNoRelationLukeAnnotatedTestExamples().keySet()) {
				String filename = id + ".json";
				JSONObject json = new JSONObject();
				json.put("id", id);
				json.put("guess", "no_relation");
				json.put("gold", this.model.getNoRelationLukeAnnotatedTestExamples().get(id));
				json.put("compatible_relations", new JSONArray());
				json.getJSONArray("compatible_relations").put("no_relation");
				Writer writer = new FileWriter(this.outputDirectory + "/" + filename);
				json.write(writer);
				writer.close();
			}
		}


		if(!this.skipAnalysis) {
			System.out.println("\nAnalyzing results...");
			AnalyzeResults.run(this.outputDirectory, this.verbose);
		}
	}

	private synchronized int getNextExampleId() {
		if(this.toBeProcessedTestExampleId == this.nbTestExamples) {
			return -1;
		}

		int returnVal = this.toBeProcessedTestExampleId;
		this.toBeProcessedTestExampleId++;
		return returnVal;
	}

	private static class ExperimentThread extends Thread {
		private final Experiment experiment;
		private final String outputDir;
		private final boolean verbose;

		private final Map<String, String> guessMap = new HashMap<>();

		public Map<String, String> getGuessMap() {
			return this.guessMap;
		}

		public ExperimentThread(Experiment experiment, String outputDir, boolean verbose) {
			this.experiment = experiment;
			this.outputDir = outputDir;
			this.verbose = verbose;
		}

		public void run() {
			RelationExtractionModel model = experiment.model;

			List<String> testPairList = null;
			try {
				testPairList = model.getTestPairList();
			} catch(RelExException e) {
				e.printStackTrace();
			}

			int count = 0;

			int nextExampleIndex = experiment.getNextExampleId();
			while(nextExampleIndex >= 0) {
				assert testPairList != null;
				String id = testPairList.get(nextExampleIndex);

				if(verbose) {
					System.out.printf("%s: [%d/%d] Processing sentence %s\n",
							Thread.currentThread().getName(),
							nextExampleIndex + 1,
							experiment.nbTestExamples,
							id);
				}

				JSONObject result = null;
				try {
					result = model.processTestExample(id);
				} catch(Exception e) {
					System.err.printf("%s: Error while processing test example %s\n", Thread.currentThread().getName(), id);
					e.printStackTrace();
				}
				if(result != null) {
					this.guessMap.put(id, result.getString("guess"));

					String fileName = outputDir + "/" + id + ".json";
					try {
						FileWriter resultWriter = new FileWriter(fileName);
						result.write(resultWriter);
						resultWriter.flush();
						resultWriter.close();
					} catch(IOException e) {
						System.err.printf("%s: Error while writing the result for %s to file\n", Thread.currentThread().getName(), id);
						e.printStackTrace();
					}
				}

				count++;
				if(count == 10) {
					count = 0;
					System.gc();
				}
				nextExampleIndex = experiment.getNextExampleId();
			}

			if(verbose) {
				System.out.printf("%s: Processing complete\n", Thread.currentThread().getName());
			}
		}
	}
}
