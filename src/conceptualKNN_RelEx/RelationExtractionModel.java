package conceptualKNN_RelEx;

import conceptualKNN.ConceptualKNNModel;
import conceptualKNN.Partition;
import conceptualKNN.utils.ConceptualKNNThread;
import conceptualKNN.utils.Table;
import conceptualKNN.utils.TableException;
import conceptualKNN_RelEx.scorer.Scorer;
import conceptualKNN_RelEx.scorer.ScorerRegistry;
import conceptualKNN_RelEx.utils.GenerateOntology;
import conceptualKNN_RelEx.utils.RelExException;
import conceptualKNN_RelEx.utils.RelExPair;
import edu.mit.jwi.Dictionary;
import edu.mit.jwi.IDictionary;
import edu.mit.jwi.item.*;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.*;
import org.apache.jena.sparql.core.Var;
import org.apache.jena.sparql.engine.binding.Binding;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author hayats
 */
public class RelationExtractionModel {
	private static boolean initialized = false;

	private static final String URI_base = "http://example.org/";
	private static final String URI_id = URI_base + "id#";
	private static final String URI_token = URI_base + "token#";
	private static final String URI_lemma = URI_base + "lemma#";
	private static final String URI_pos = URI_base + "pos#";
	private static final String URI_ner = URI_base + "ner#";
	private static final String URI_deprel = URI_base + "deprel#";
	private static final String URI_synset = URI_base + "synset#";
	private static final Map<Character, String> PUNCT_CONVERSION_TABLE = new HashMap<>();

	private static final Set<String> POSTAGS_VERB = new HashSet<>();
	private static final Set<String> POSTAGS_NOUN = new HashSet<>();

	private final Map<String, Map<String, RelExPair>> importedTrainingPairs = new HashMap<>();
	private final Map<String, RelExPair> importedTestPairs = new HashMap<>();
	private final ConceptualKNNModel model;

	private final IDictionary wordNetDict;
	private final Map<POS, Set<String>> lemmas = new HashMap<>();
	private final Set<String> synsetSet = new HashSet<>();
	private final Set<Statement> synsetOnto = new HashSet<>();

	private final Map<String, Map<String, Set<String>>> compatibilityRelationTable = new HashMap<>();
	private final Map<String, Integer> relationTypeCounter = new HashMap<>();

	private final List<Scorer> scorers = new ArrayList<>();

	private List<String> testPairList = null;

	private final Map<String, String> noRelationLukeAnnotatedTestExamples = new HashMap<>();

	private String dotFilesDir = "";

	private final boolean pruneDependencyTree;
	private final boolean verbose;
	private final boolean useLukeAnnotation;
	private final int maxComputationTime;
	private final boolean saturateIntensions;

	private boolean importPhase;

	private static void init() {
		if(!initialized) {
			initialized = true;

			/* Punctuation conversion table */
			PUNCT_CONVERSION_TABLE.put('.', "_DOT_");
			PUNCT_CONVERSION_TABLE.put(',', "%2C");
			PUNCT_CONVERSION_TABLE.put(':', "%3A");
			PUNCT_CONVERSION_TABLE.put(';', "%3B");
			PUNCT_CONVERSION_TABLE.put('?', "%3F");
			PUNCT_CONVERSION_TABLE.put('!', "_EXCLAMATIONMARK_");
			PUNCT_CONVERSION_TABLE.put('#', "%23");
			PUNCT_CONVERSION_TABLE.put('-', "_HYPHEN_");
			PUNCT_CONVERSION_TABLE.put('\'', "%22");
			PUNCT_CONVERSION_TABLE.put('\"', "%22");
			PUNCT_CONVERSION_TABLE.put('`', "%60");
			PUNCT_CONVERSION_TABLE.put('@', "%40");
			PUNCT_CONVERSION_TABLE.put('>', "%3E");
			PUNCT_CONVERSION_TABLE.put('<', "%3C");
			PUNCT_CONVERSION_TABLE.put('$', "%24");
			PUNCT_CONVERSION_TABLE.put('€', "_EURO_");
			PUNCT_CONVERSION_TABLE.put('£', "_POUND_");
			PUNCT_CONVERSION_TABLE.put('|', "%7C");
			PUNCT_CONVERSION_TABLE.put('\\', "%5C");
			PUNCT_CONVERSION_TABLE.put('%', "%25");
			PUNCT_CONVERSION_TABLE.put('&', "%26");
			PUNCT_CONVERSION_TABLE.put('*', "_ASTERISK_");
			PUNCT_CONVERSION_TABLE.put('^', "%5E");
			PUNCT_CONVERSION_TABLE.put('+', "%2B");
			PUNCT_CONVERSION_TABLE.put('/', "%2F");
			PUNCT_CONVERSION_TABLE.put('=', "%3D");
			PUNCT_CONVERSION_TABLE.put(' ', "%20");
			PUNCT_CONVERSION_TABLE.put('{', "%7B");
			PUNCT_CONVERSION_TABLE.put('}', "%7D");
			PUNCT_CONVERSION_TABLE.put('~', "%7E");
			PUNCT_CONVERSION_TABLE.put('[', "%5B");
			PUNCT_CONVERSION_TABLE.put(']', "%5D");

			POSTAGS_NOUN.add("NN");
			POSTAGS_NOUN.add("NNS");

			POSTAGS_VERB.add("VB");
			POSTAGS_VERB.add("VBD");
			POSTAGS_VERB.add("VBG");
			POSTAGS_VERB.add("VBN");
			POSTAGS_VERB.add("VBP");
			POSTAGS_VERB.add("VBZ");
		}
	}

	/**
	 * Create an empty model.
	 */
	public RelationExtractionModel(boolean pruneDependencyTree,
								   boolean verbose,
								   boolean useLukeAnnotation,
								   int computationTime,
								   String dictionaryPath,
								   List<String> scorers,
								   boolean saturateIntensions) throws IOException, RelExException {

		/* Initialisation */
		init();
		this.pruneDependencyTree = pruneDependencyTree;
		this.verbose = verbose;
		this.useLukeAnnotation = useLukeAnnotation;
		this.maxComputationTime = computationTime;
		this.importPhase = true;
		this.saturateIntensions = saturateIntensions;

		this.model = new ConceptualKNNModel();
		Model rdfModel = this.model.getModel();

		/* Prefixes */
		rdfModel.setNsPrefix("id", URI_id);
		rdfModel.setNsPrefix("token", URI_token);
		rdfModel.setNsPrefix("lemma", URI_lemma);
		rdfModel.setNsPrefix("synset", URI_synset);
		rdfModel.setNsPrefix("pos", URI_pos);
		rdfModel.setNsPrefix("ner", URI_ner);
		rdfModel.setNsPrefix("deprel", URI_deprel);

		/* Setting WordNet dictionnary */
		URL url = new URL("file", null, dictionaryPath);
		this.wordNetDict = new Dictionary(url);
		this.wordNetDict.open();

		this.lemmas.put(POS.NOUN, new HashSet<>());
		this.lemmas.put(POS.VERB, new HashSet<>());

		/* Setting up scorers */
		for(String s : scorers) {
			this.scorers.add(ScorerRegistry.getScorer(s));
		}

		/* Adding POSTag ontology to the RDF model */
		Model postagOnto = GenerateOntology.generate();
		postagOnto.listStatements().forEachRemaining(model::add);
	}

	public long getModelSize() {
		return this.model.getModel().size();
	}

	/**
	 * Compute the Table containing the couples subject-object of the imported sentences
	 */
	public Table getTrainingPairTable() throws TableException {
		Table ret = null;
		for(String relation : this.importedTrainingPairs.keySet()) {
			for(String id : this.importedTrainingPairs.get(relation).keySet()) {
				Binding binding = this.importedTrainingPairs.get(relation).get(id).getBinding();
				if(ret == null) {
					List<Var> schema = new ArrayList<>();
					binding.vars().forEachRemaining(schema::add);
					ret = new Table(schema);
				}
				ret.addBinding(binding);
			}
		}
		return ret;
	}

	public Integer getRelationTypeCounter(String relation) {
		return this.relationTypeCounter.get(relation);
	}

	public List<String> getTrainingPairList() {
		List<String> ret = new ArrayList<>();
		for(String relation : this.importedTrainingPairs.keySet()) {
			ret.addAll(this.importedTrainingPairs.get(relation).keySet());
		}
		ret.sort(Comparator.comparing(String::toString));
		return ret;
	}

	public List<String> getTestPairList() throws RelExException {
		if(this.importPhase) {
			throw new RelExException("Import phase not closed: cannot access the list of test pairs");
		}

		return this.testPairList;
	}

	public RelExPair getTrainingPair(String pairId) {
		for(String relation : this.importedTrainingPairs.keySet()) {
			for(String id : this.importedTrainingPairs.get(relation).keySet()) {
				if(id.equals(pairId)) {
					return this.importedTrainingPairs.get(relation).get(id);
				}
			}
		}
		return null;
	}

	public RelExPair getTestPair(String id) {
		return this.importedTestPairs.get(id);
	}

	private Table getCompatibleTrainingPairTable(RelExPair testPair) throws TableException {
		Table ret = null;
		for(String relation : this.compatibilityRelationTable.get(testPair.getSubjectType()).get(testPair.getObjectType())) {
			for(String id : importedTrainingPairs.get(relation).keySet()) {
				Binding binding = this.importedTrainingPairs.get(relation).get(id).getBinding();
				if(ret == null) {
					List<Var> schema = new ArrayList<>();
					binding.vars().forEachRemaining(schema::add);
					ret = new Table(schema);
				}
				ret.addBinding(binding);
			}
		}
		return ret;
	}

	public Set<String> getCompatibleRelations(RelExPair testPair) {
		if(this.compatibilityRelationTable.containsKey(testPair.getSubjectType()) && this.compatibilityRelationTable.get(testPair.getSubjectType()).containsKey(testPair.getObjectType())) {
			return this.compatibilityRelationTable.get(testPair.getSubjectType()).get(testPair.getObjectType());
		} else {
			return new HashSet<>();
		}
	}

	/**
	 * Convert a whole tacred-formatted JSONArray of sentences into a RDF representation that is added to the model.
	 *
	 * @param tacred_json a tacred-formated JSONArray corresponding to a set of sentences to convert.
	 */
	public void importDatabase(JSONArray tacred_json, boolean isTraining) throws RelExException, IOException {
		if(!this.importPhase) {
			throw new RelExException("Import phase is over, model has been processed for the processing phase");
		}

		int length = tacred_json.length();
		for(int i = 0; i < length; i++) {
			JSONObject sentence = tacred_json.getJSONObject(i);
			if(verbose) {
				System.out.printf("[%d/%d] Importing: %s (%f%%)\n", i + 1, length, sentence.getString("id"), (i + 1) * 100.0 / length);
			}

			if(!isTraining) {
				if(this.useLukeAnnotation) {
					if(sentence.getString("luke_annotation").equals("no_relation")) {
						if(verbose) {
							System.out.println("No relation: skipping...");
						}
						this.noRelationLukeAnnotatedTestExamples.put(sentence.getString("id"), sentence.getString("relation"));
					} else {
						this.importSentence(sentence, false, verbose);
					}
				} else {
					if(sentence.getString("relation").equals("no_relation")) {
						if(verbose) {
							System.out.println("No relation: skipping...");
						}
					} else {
						this.importSentence(sentence, false, verbose);
					}
				}
			} else {
				if(sentence.getString("relation").equals("no_relation")) {
					if(verbose) {
						System.out.println("No relation: skipping...");
					}
				} else {
					this.importSentence(sentence, true, verbose);
				}
			}
		}
	}

	/**
	 * Add a sentence to the model
	 *
	 * @param tacred_sentence A sentence as a Tacred-formatted JSONObject
	 */
	private void importSentence(JSONObject tacred_sentence, boolean isTraining, boolean verbose) throws RelExException, IOException {
		if(!this.importPhase) {
			throw new RelExException("Import phase is over, model has been processed for the processing phase");
		}

		if(this.importedTrainingPairs.containsKey(tacred_sentence.getString("id"))
				|| this.importedTestPairs.containsKey(tacred_sentence.getString("id"))) {
			return;
		}

		List<Statement> statements = new ArrayList<>();

		String sentence_id = tacred_sentence.getString("id");

		JSONArray tokens = tacred_sentence.getJSONArray("token");
		JSONArray lemmas = tacred_sentence.getJSONArray("lemma");
		JSONArray pos = tacred_sentence.getJSONArray("stanford_pos");
		JSONArray ner = tacred_sentence.getJSONArray("stanford_ner");
		JSONArray head = tacred_sentence.getJSONArray("stanford_head");
		JSONArray deprel = tacred_sentence.getJSONArray("stanford_deprel");

		int subj_start_idx = tacred_sentence.getInt("subj_start");
		int subj_end_idx = tacred_sentence.getInt("subj_end");
		String subj_type = tacred_sentence.getString("subj_type");
		int obj_start_idx = tacred_sentence.getInt("obj_start");
		int obj_end_idx = tacred_sentence.getInt("obj_end");
		String obj_type = tacred_sentence.getString("obj_type");

		String tacred_rel_str = tacred_sentence.getString("relation");

		/*
		 * Pre-process the JSON to have consistent head values
		 */
		for(int i = 0; i < tokens.length(); i++) {
			int headVal = head.getInt(i) - 1;
			head.put(i, headVal);
		}

		/*
		 * Pre-process the JSON to merge the split named entities in one token
		 */
		ArrayList<Integer> toBeDeleted = new ArrayList<>();
		ArrayList<Integer> namedEntityStarts = new ArrayList<>();
		ArrayList<Integer> namedEntityEnds = new ArrayList<>();

		/* identify the named entities start and end */
		int start = -1;
		String previousNerType = "O";
		for(int i = 0; i < tokens.length(); i++) {
			if(!ner.getString(i).equals("O")) {
				if(previousNerType.equals("O")) {
					previousNerType = ner.getString(i);
					start = i;
				} else if(!previousNerType.equals(ner.getString(i))) {
					namedEntityStarts.add(start);
					namedEntityEnds.add(i);
					previousNerType = ner.getString(i);
					start = i;
				}
			} else {
				if(!previousNerType.equals("O")) {
					namedEntityStarts.add(start);
					namedEntityEnds.add(i);
					previousNerType = "O";
				}
			}
		}
		if(!previousNerType.equals("O")) {
			namedEntityStarts.add(start);
			namedEntityEnds.add(tokens.length());
		}

		/* For each named entity */
		for(int i = 0; i < namedEntityStarts.size(); i++) {
			int root = -1;
			int rootCount = 0;
			int begin = namedEntityStarts.get(i);
			int end = namedEntityEnds.get(i);
			for(int j = begin; j < end; j++) { // Count the number of roots
				int headVal = head.getInt(j);
				if(headVal < begin || headVal >= end) {
					root = j;
					rootCount++;
				}
			}
			if(rootCount >= 2) { // More than one roots: syntactically inconsistent named entity, cannot be handled
				if(isTraining) {
					if(verbose) {
						System.out.println("inconsistent named entities: skipping the sentence...");
					}
					return;
				}
			} else { // Well-formed named entity
				/* Merge the tokens of the entity */
				String mergedTokens = "";
				for(int j = begin; j < end; j++) {
					if(!mergedTokens.equals("")) {
						mergedTokens = mergedTokens.concat("_");
					}
					mergedTokens = mergedTokens.concat(tokens.getString(j));
				}
				tokens.put(root, mergedTokens);
				/* Update head values */
				for(int j = 0; j < tokens.length(); j++) {
					int headVal = head.getInt(j);
					if(headVal >= begin && headVal < end) {
						head.put(j, root);
					}
				}
				/* Update Tacred subjects & objects start & end if needed */
				if(subj_start_idx >= begin && subj_end_idx < end) {
					subj_start_idx = root;
				}
				if(subj_end_idx >= begin && subj_end_idx < end) {
					subj_end_idx = root;
				}
				if(obj_start_idx >= begin && obj_end_idx < end) {
					obj_start_idx = root;
				}
				if(obj_end_idx >= begin && obj_end_idx < end) {
					obj_end_idx = root;
				}
				/* Add non-root tokens to the deletion list */
				for(int j = begin; j < end; j++) {
					if(j != root) {
						toBeDeleted.add(j);
					}
				}
			}
		}

		/* Delete the tokens to be deleted */
		for(int i = 0; i < toBeDeleted.size(); i++) {
			int line = toBeDeleted.get(i);

			/* Delete lines in JSONArrays */
			tokens.remove(line);
			pos.remove(line);
			ner.remove(line);
			head.remove(line);
			deprel.remove(line);
			lemmas.remove(line);

			/* Update head values */
			for(int j = 0; j < tokens.length(); j++) {
				int headVal = head.getInt(j);
				if(headVal > line) {
					head.put(j, headVal - 1);
				}
			}
			if(subj_start_idx > line) {
				subj_start_idx--;
			}
			if(subj_end_idx > line) {
				subj_end_idx--;
			}
			if(obj_start_idx > line) {
				obj_start_idx--;
			}
			if(obj_end_idx > line) {
				obj_end_idx--;
			}

			/* Update lines to be deleted */
			for(int j = i; j < toBeDeleted.size(); j++) {
				int lj = toBeDeleted.get(j);
				if(lj > line) {
					toBeDeleted.set(j, lj - 1);
				}
			}
		}

		/*
		 * Preprocess the data to remove special characters
		 */
		for(int i = 0; i < tokens.length(); i++) {
			tokens.put(i, this.str_preprocessing_tokens(tokens.getString(i)));
			lemmas.put(i, this.str_preprocessing(lemmas.getString(i)));
			pos.put(i, this.str_preprocessing(pos.getString(i)));
			ner.put(i, this.str_preprocessing(ner.getString(i)));
			deprel.put(i, this.str_preprocessing(deprel.getString(i)));
		}

		/*
		 * Identify subject and object
		 */
		int subjRoot = subj_start_idx;
		if(subj_end_idx != subj_start_idx) {
			int rootCount = 0;
			ArrayList<Integer> roots = new ArrayList<>();
			for(int i = subj_start_idx; i <= subj_end_idx; i++) {
				int val = i;
				while(head.getInt(val) != -1
						&& head.getInt(val) != val
						&& head.getInt(val) >= subj_start_idx
						&& head.getInt(val) <= subj_end_idx) {
					val = head.getInt(val);
				}
				if(!roots.contains(val)) {
					roots.add(val);
					rootCount++;
				}
			}
			if(rootCount > 1) {
				if(isTraining) {
					if(verbose) {
						System.out.println("Split subject: skipping sentence");
					}
					return;
				}
			}
			subjRoot = roots.get(0);
		}
		int objRoot = obj_start_idx;
		if(obj_end_idx != obj_start_idx) {
			int rootCount = 0;
			ArrayList<Integer> roots = new ArrayList<>();
			for(int i = obj_start_idx; i <= obj_end_idx; i++) {
				int val = i;
				while(head.getInt(val) != -1
						&& head.getInt(val) != val
						&& head.getInt(val) >= obj_start_idx
						&& head.getInt(val) <= obj_end_idx) {
					val = head.getInt(val);
				}
				if(!roots.contains(val)) {
					roots.add(val);
					rootCount++;
				}
			}
			if(rootCount > 1) {
				if(isTraining) {
					if(verbose) {
						System.out.println("Split object: skipping sentence");
					}
					return;
				}
			}
			objRoot = roots.get(roots.size() - 1);
		}

		/*
		 * Prune the dependency tree if needed
		 */
		Set<Integer> toBeModeled = new HashSet<>();
		if(this.pruneDependencyTree) {
			/*
			 * Computation of the path between the subject and the object
			 */
			List<Integer> subjAncestors = new ArrayList<>();
			List<Integer> objAncestors = new ArrayList<>();

			int current = subjRoot;
			do {
				subjAncestors.add(current);
				current = head.getInt(current);
			} while(current != -1);

			current = objRoot;
			do {
				objAncestors.add(current);
				current = head.getInt(current);
			} while(current != -1);

			int i = 0;
			int j = 0;
			int commonAncestor = -1;
			while(commonAncestor == -1 && i < subjAncestors.size()) {
				while(commonAncestor == -1 && j < objAncestors.size()) {
					if(subjAncestors.get(i).equals(objAncestors.get(j))) {
						commonAncestor = subjAncestors.get(i);
					} else {
						j++;
					}
				}
				if(commonAncestor == -1) {
					i++;
					j = 0;
				}
			}

			/*
			 * Selection of the tokens to add to the model
			 */
			if(commonAncestor == -1) {
				toBeModeled.addAll(subjAncestors);
				toBeModeled.addAll(objAncestors);
			} else {
				i = 0;
				while(subjAncestors.get(i) != commonAncestor) {
					toBeModeled.add(subjAncestors.get(i));
					i++;
				}
				j = 0;
				while(objAncestors.get(j) != commonAncestor) {
					toBeModeled.add(objAncestors.get(j));
					j++;
				}

				toBeModeled.add(commonAncestor);
				if(head.getInt(commonAncestor) != -1) {
					toBeModeled.add(head.getInt(commonAncestor));
				}
			}

			Set<Integer> neighbourhood = new HashSet<>();
			for(i = 0; i < tokens.length(); i++) {
				if(!neighbourhood.contains(i) && !toBeModeled.contains(i) && toBeModeled.contains(head.getInt(i))) {
					neighbourhood.add(i);
				}
			}
			toBeModeled.addAll(neighbourhood);
		} else {
			// If not pruned, all tokens must be conserved
			for(int i = 0; i < tokens.length(); i++) {
				toBeModeled.add(i);
			}
		}

		/*
		 * Deletion of the punctuation tokens which are leaf
		 */
		Set<Integer> punctLeafTokens = new HashSet<>();
		// Select the punctuation tokens
		for(int token : toBeModeled) {
			if(deprel.getString(token).equals("punct") && token != subjRoot && token != objRoot) {
				punctLeafTokens.add(token);
			}
		}
		// Remove the punctuations that are not leaves from the set of punctuations
		for(int token : toBeModeled) {
			int parent = head.getInt(token);
			punctLeafTokens.remove(parent);
		}
		// Remove the punctuation leaves from the set of conserved tokens
		for(int token : punctLeafTokens) {
			toBeModeled.remove(token);
		}

		/*
		 * Add token, lemma, POSTag & NERTag (if exists) as types to each token id
		 */
		Map<Integer, Resource> id_resources = new HashMap<>();

		for(int i : toBeModeled) {
			String id = sentence_id + "_" + i + "_" + this.str_preprocessing_tokens(tokens.getString(i));
			id_resources.put(i, this.model.createResource(RelationExtractionModel.URI_id + id));

			String pos_str = pos.getString(i);
			Resource pos_resource = this.model.createResource(RelationExtractionModel.URI_pos + pos_str);
			Statement stmt = this.model.createStatement(id_resources.get(i), RDF.type, pos_resource);
			statements.add(stmt);

			if(!(ner.getString(i)).equals("O")) {
				Resource ner_resource = this.model.createResource(RelationExtractionModel.URI_ner + ner.getString(i));
				stmt = this.model.createStatement(id_resources.get(i), RDF.type, ner_resource);
				statements.add(stmt);

				String token_str = tokens.getString(i);
				Resource token_resource = this.model.createResource(RelationExtractionModel.URI_token + token_str);
				stmt = this.model.createStatement(id_resources.get(i), RDF.type, token_resource);
				statements.add(stmt);
			} else {
				String lemma_str = lemmas.getString(i);
				Resource lemma_resource = this.model.createResource(RelationExtractionModel.URI_lemma + lemma_str);
				stmt = this.model.createStatement(id_resources.get(i), RDF.type, lemma_resource);
				statements.add(stmt);

				if(POSTAGS_VERB.contains(pos_str)) {
					addLemmaOntology(lemma_str, POS.VERB);
				} else if(POSTAGS_NOUN.contains(pos_str)) {
					addLemmaOntology(lemma_str, POS.NOUN);
				}
			}
		}

		/*
		 * Add syntactic relations between token ids
		 */
		for(int i : toBeModeled) {
			String deprel_name = deprel.getString(i);
			int parent = head.getInt(i);
			if(toBeModeled.contains(parent) && !deprel_name.equals("ROOT")) {
				Property deprel_property = this.model.createProperty(RelationExtractionModel.URI_deprel, "has_" + deprel_name);
				int head_index = head.getInt(i);
				Statement stmt = model.createStatement(id_resources.get(head_index), deprel_property, id_resources.get(i));
				statements.add(stmt);
			}
		}

		/*
		 * Identifies TACRED subject & object
		 */
		Resource subject_res = id_resources.get(subjRoot);
		Resource object_res = id_resources.get(objRoot);

		RelExPair pair = new RelExPair(subject_res, object_res, tacred_rel_str, sentence_id, subj_type, obj_type);

		if(isTraining) {
			if(!this.importedTrainingPairs.containsKey(tacred_rel_str)) {
				this.importedTrainingPairs.put(tacred_rel_str, new HashMap<>());
			}
			this.importedTrainingPairs.get(tacred_rel_str).put(sentence_id, pair);
		} else {
			this.importedTestPairs.put(sentence_id, pair);
		}
		this.model.add(statements);
		if(!dotFilesDir.equals("")) {
			this.generateDotFile(statements, sentence_id);
		}

		/*
		 * Add the relation to the compatibility table
		 */
		if(isTraining) {
			if(!this.compatibilityRelationTable.containsKey(subj_type)) {
				this.compatibilityRelationTable.put(subj_type, new HashMap<>());
			}
			if(!this.compatibilityRelationTable.get(subj_type).containsKey(obj_type)) {
				this.compatibilityRelationTable.get(subj_type).put(obj_type, new HashSet<>());
			}
			this.compatibilityRelationTable.get(subj_type)
					.get(obj_type)
					.add(tacred_rel_str);

			this.relationTypeCounter.put(tacred_rel_str, this.relationTypeCounter.getOrDefault(tacred_rel_str, 0) + 1);
		}
	}

	private void generateDotFile(List<Statement> statements, String sentenceId) throws IOException {
		StringBuilder dotfile = new StringBuilder("digraph {\n");
		for(Statement statement : statements) {
			String subjectFull = statement.getSubject().getURI();
			String objectFull = statement.getObject().toString();
			String labelFull = statement.getPredicate().getURI();
			String subject = subjectFull.substring(subjectFull.lastIndexOf('/') + 1);
			String object = objectFull.substring(objectFull.lastIndexOf('/') + 1);
			String label = labelFull.substring(labelFull.lastIndexOf('#') + 1);

			dotfile.append(String.format("\t\"%s\" -> \"%s\" [label = \"%s\"]\n", subject, object, label));
		}
		dotfile.append("}\n");

		FileWriter writer = new FileWriter(this.dotFilesDir + "/" + sentenceId + ".dot");
		writer.write(dotfile.toString());
		writer.flush();
		writer.close();
	}

	private void addLemmaOntology(String lemma, POS pos) {
		if(!this.lemmas.get(pos).contains(lemma)) {
			IIndexWord wordIndex = this.wordNetDict.getIndexWord(lemma, pos);
			Resource lemmaRes = this.model.createResource(URI_lemma + lemma);

			if(wordIndex != null) {
				wordIndex.getWordIDs().forEach(id -> {
					IWord word = this.wordNetDict.getWord(id);
					ISynset synset = word.getSynset();
					ISynsetID synsetID = synset.getID();

					String synsetStr = synsetID.getPOS().toString() + synsetID.getOffset() + "_" + synset.getWords().get(0);

					Resource synsetRes = this.model.createResource(URI_synset + synsetStr);
					Statement statement = this.model.createStatement(lemmaRes, RDFS.subClassOf, synsetRes);

					this.synsetOnto.add(statement);
					this.addSynsetOntology(synset);
				});
			}

			this.lemmas.get(pos).add(lemma);
		}
	}

	private void addSynsetOntology(ISynset synset) {
		ISynsetID synsetID = synset.getID();
		String synsetStr = synsetID.getPOS().toString() + synsetID.getOffset() + "_" + synset.getWords().get(0).getLemma();
		Resource synsetRes = this.model.createResource(URI_synset + synsetStr);

		if(!this.synsetSet.contains(synsetStr)) {
			this.synsetSet.add(synsetStr);

			synset.getRelatedSynsets(Pointer.HYPERNYM).forEach(hypernymId -> {
				ISynset hypernymSynset = this.wordNetDict.getSynset(hypernymId);
				String hypernymStr = hypernymId.getPOS().toString() + hypernymId.getOffset() + "_" + hypernymSynset.getWords().get(0).getLemma();
				Resource hypernymRes = this.model.createResource(URI_synset + hypernymStr);

				Statement statement = this.model.createStatement(synsetRes, RDFS.subClassOf, hypernymRes);
				this.synsetOnto.add(statement);

				this.addSynsetOntology(hypernymSynset);
			});
		}
	}

	private String str_preprocessing_tokens(String str) {
		String str_ret = "";
		for(int i = 0; i < str.length(); i++) {
			if(PUNCT_CONVERSION_TABLE.containsKey(str.charAt(i))) {
				str_ret = str_ret.concat(PUNCT_CONVERSION_TABLE.get(str.charAt(i)));
			} else {
				str_ret = str_ret.concat(String.valueOf(Character.toLowerCase(str.charAt(i))));
			}
		}
		return str_ret;
	}

	private String str_preprocessing(String str) {
		String str_ret = "";
		for(int i = 0; i < str.length(); i++) {
			if(PUNCT_CONVERSION_TABLE.containsKey(str.charAt(i))) {
				str_ret = str_ret.concat(PUNCT_CONVERSION_TABLE.get(str.charAt(i)));
			} else {
				str_ret = str_ret.concat(String.valueOf(str.charAt(i)));
			}
		}
		return str_ret;
	}

	public JSONObject processTestExample(String id) throws InterruptedException, RelExException, TableException {
		if(this.importPhase) {
			throw new RelExException("Please conclude import phase with setImportFinished() before processing test examples");
		}

		RelExPair pair = this.getTestPair(id);
		String gold = pair.getRelation();
		String guess;
		JSONObject results;

		Set<String> compatibleRelations = this.getCompatibleRelations(pair);

		if(compatibleRelations.size() == 0) {
			guess = "no_relation";
			results = new JSONObject();
		} else if(compatibleRelations.size() == 1) {
			guess = compatibleRelations.iterator().next();
			results = new JSONObject();
		} else {
			guess = "";
			/*
			 * Computation of the CNN for this example
			 */
			Table table = this.getCompatibleTrainingPairTable(pair);
			Partition partition = new Partition(this.model, pair.getUris(), table, -1);
			AtomicBoolean cut = new AtomicBoolean(false);
			ConceptualKNNThread thread = new ConceptualKNNThread(partition, cut);
			thread.start();
			thread.join(this.maxComputationTime * 1000L);
			cut.set(true);
			thread.join(3000);
			if(thread.isAlive()) {
				System.err.printf("%s: interrupting thread...\n", Thread.currentThread().getName());
				thread.stop();
			}

			if(this.saturateIntensions) {
				partition.saturateIntensions();
			}
			results = new JSONObject(new JSONTokener(partition.toJson()));

			results.put("computation_time", (float) thread.getExecTime() / 1000000000);


			/*
			 * Computation of the scores
			 */
			JSONObject jsonScorers = new JSONObject();
			boolean firstScorer = true;
			for(Scorer scorer : this.scorers) {
				JSONArray scores = scorer.score(this, partition);

				jsonScorers.put(scorer.toString(), scores);

				if(firstScorer) {
					guess = scores.getJSONObject(0).getString("relation");
					firstScorer = false;
				}
			}
			results.put("scorers", jsonScorers);
		}

		results.put("gold", gold);
		results.put("guess", guess);
		results.put("id", id);

		for(String relation : compatibleRelations) {
			results.append("compatible_relations", relation);
		}

		return results;
	}

	public int getSynsetOntoSize() {
		return this.synsetOnto.size();
	}

	public int getLemmaCount() {
		return this.lemmas.get(POS.NOUN).size() + this.lemmas.get(POS.VERB).size();
	}

	public int getSynsetCount() {
		return this.synsetSet.size();
	}

	public Map<String, String> getNoRelationLukeAnnotatedTestExamples() {
		return noRelationLukeAnnotatedTestExamples;
	}

	private void addSynsetOntology() {
		this.synsetOnto.forEach(this.model::add);
	}

	public void setImportFinished() throws RelExException {
		if(!this.importPhase) {
			throw new RelExException("The import phase has already been closed");
		}

		if(this.importedTestPairs.isEmpty()) {
			throw new RelExException("No test database has been imported to the model");
		}

		if(this.importedTrainingPairs.isEmpty()) {
			throw new RelExException("No training database has been imported to this model");
		}

		this.importPhase = false;
		this.wordNetDict.close();
		this.addSynsetOntology();
		this.testPairList = new ArrayList<>(this.importedTestPairs.keySet());
		Collections.shuffle(this.testPairList);
	}

	public void setDotFilesDir(String path) {
		this.dotFilesDir = path;
	}

	public Set<String> executeQuery(Query query) {
		ConceptualKNNModel model = this.model;
		InfModel infModel = model.getModel();

		QueryExecution queryExec = QueryExecutionFactory.create(query, infModel);

		ResultSet results = queryExec.execSelect();

		if(results.getResultVars().size() == 2) {
			List<String> subjectAnswers = new ArrayList<>();
			List<String> objectAnswers = new ArrayList<>();

			while(results.hasNext()) {
				QuerySolution solution = results.next();
				String subjectAnswer = solution.get("Subject").asNode().getURI();
				String objectAnswer = solution.get("Object").asNode().getURI();
				subjectAnswers.add(subjectAnswer);
				objectAnswers.add(objectAnswer);
			}

			Set<String> matchingSentences = new HashSet<>();

			for(int i = 0; i < objectAnswers.size(); i++) {
				String subjAns = subjectAnswers.get(i);
				String objAns = objectAnswers.get(i);

				int sharp = 0;
				int underscore = 0;
				for(int j = 0; j < subjAns.length(); j++) {
					if(sharp == 0 && subjAns.charAt(i) == '#') {
						sharp = j;
					}
					if(underscore == 0 && subjAns.charAt(i) == '_') {
						underscore = j;
					}
				}
				String sentenceId = subjAns.substring(sharp + 1, underscore);
				RelExPair pair = this.getTrainingPair(sentenceId);

				if(pair != null) {
					if(subjAns.equals(pair.getSubject().getURI()) && objAns.equals(pair.getObject().getURI())) {
						matchingSentences.add(sentenceId);
					}
				}
			}

			return matchingSentences;

		} else { // results.getResultVar.size() == 1
			String var = results.getResultVars().get(0); // "Subject" or "Object"

			List<String> answers = new ArrayList<>();
			while(results.hasNext()) {
				QuerySolution solution = results.next();
				String answer = solution.get(var).asNode().getURI();
				answers.add(answer);
			}

			Set<String> matchingSentences = new HashSet<>();

			for(String ans : answers) {
				int sharp = 0;
				int underscore = 0;
				for(int i = 0; i < ans.length(); i++) {
					if(sharp == 0 && ans.charAt(i) == '#') {
						sharp = i;
					}
					if(underscore == 0 && ans.charAt(i) == '_') {
						underscore = i;
					}
				}
				String sentenceId = ans.substring(sharp + 1, underscore);
				RelExPair pair = this.getTrainingPair(sentenceId);

				if(pair != null) {
					if((var.equals("Subject") && ans.equals(pair.getSubject().getURI()) || (var.equals("Object") && ans.equals(pair.getObject().getURI())))) {
						matchingSentences.add(sentenceId);
					}
				}
			}

			return matchingSentences;
		}


	}
}