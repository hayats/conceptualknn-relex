package conceptualKNN_RelEx.utils;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentContainer;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GraphMDLConverter {
	public static final String DESCRIPTION = "GraphMDL Converter";

	public static void addCliArgs(ArgumentContainer parser) {
		parser.description(DESCRIPTION);

		parser.addArgument("conceptsFile")
				.type(Arguments.fileType().verifyIsFile())
				.help("path to the conceptsOfNeighbours file");

		parser.addArgument("outputFile")
				.help("Path to the output file");
	}

	public static void main(String[] args) throws IOException {
		ArgumentParser parser = ArgumentParsers.newFor(AnalyzeResults.class.getName()).build();
		addCliArgs(parser);

		try {
			execute(parser.parseArgs(args));
		} catch(ArgumentParserException e) {
			parser.handleError(e);
			System.exit(1);
		}
	}

	public static void execute(Namespace namespace) throws FileNotFoundException {
		JSONObject result = new JSONObject(new JSONTokener(new FileInputStream(namespace.getString("conceptsFile"))));

		JSONArray compatRel = result.getJSONArray("compatible_relations");
		String prediction;
		if(compatRel.length() == 1 || result.getString("guess").equals("no_relation")) {
			prediction = result.getString("guess");
		} else {
			prediction = result.getJSONObject("scorers")
					.getJSONArray("maximum_confidence_bis")
					.getJSONObject(0)
					.getString("relation");
		}

		if(!prediction.equals("no_relation") && compatRel.length() > 1) {

			JSONArray score = result.getJSONObject("scorers")
					.getJSONArray("maximum_confidence_bis")
					.getJSONObject(0)
					.getJSONArray("score");

			int nbrConf1 = 0;
			while(score.getJSONObject(nbrConf1).getDouble("score") == 1) {
				nbrConf1++;
			}

			Map<Integer, JSONObject> idToConcept = new HashMap<>();
			JSONArray concepts = result.getJSONArray("conceptsOfNeighbours");
			for(int i = 0; i < concepts.length(); i++) {
				JSONObject concept = concepts.getJSONObject(i);
				int id = concept.getInt("id");
				idToConcept.put(id, concept);
			}

			List<List<String>> intensions = new ArrayList<>();
			for(int i = 0; i < nbrConf1; i++) {
				int id = score.getJSONObject(i).getInt("id");
				JSONObject concept = idToConcept.get(id);

				JSONArray intension = concept.getJSONArray("intension");
				List<String> intensionList = new ArrayList<>();
				for(int j = 0; j < intension.length(); j++) {
					intensionList.add(intension.getString(j));
				}
				intensionList.sort(String::compareTo);
				intensions.add(intensionList);
			}

			PrintWriter writer = new PrintWriter(namespace.getString("outputFile"));
			for(List<String> intension : intensions) {
				int nextVertexId = 0;
				Map<String, Integer> vertexIds = new HashMap<>();
				Map<Integer, List<String>> vertexLabels = new HashMap<>();
				Map<Integer, Map<Integer, String>> edges = new HashMap<>();
				for(String triple : intension) {
					int i = 0;
					while(!(triple.charAt(i) == ' ')) {
						i++;
					}
					int j = i;
					while(triple.charAt(j) == ' ') {
						j++;
					}
					int k = j;
					while(!(triple.charAt(k) == ' ')) {
						k++;
					}
					int l = k;
					while(triple.charAt(l) == ' ') {
						l++;
					}
					String subject = triple.substring(0, i);
					String predicate = triple.substring(j, k);
					String object = triple.substring(l);
					/*
					System.out.printf("Subj: %s\n", subject);
					System.out.printf("Pred: %s\n", predicate);
					System.out.printf("Obj: %s\n", object);*/

					if(!vertexIds.containsKey(subject)) {
						vertexIds.put(subject, nextVertexId);
						vertexLabels.put(nextVertexId, new ArrayList<>());
						vertexLabels.get(nextVertexId).add(subject);
						nextVertexId++;
					}

					int subjId = vertexIds.get(subject);
					if(predicate.equals("a")) {
						/*
						vertexLabels.put(nextVertexId, new ArrayList<>());
						vertexLabels.get(nextVertexId).add(object);
						int objId = nextVertexId;
						nextVertexId++;

						if(!edges.containsKey(subjId)) {
							edges.put(subjId, new HashMap<>());
						}
						edges.get(subjId).put(objId, "rdf:type");
						 */
						vertexLabels.get(subjId).add(object);
					} else {
						if(!vertexIds.containsKey(object)) {
							vertexIds.put(object, nextVertexId);
							vertexLabels.put(nextVertexId, new ArrayList<>());
							vertexLabels.get(nextVertexId).add(object);
							nextVertexId++;
						}

						int objId = vertexIds.get(object);

						if(!edges.containsKey(subjId)) {
							edges.put(subjId, new HashMap<>());
						}
						edges.get(subjId).put(objId, predicate);
					}
				}

				writer.println("t");
				for(int vertexId : vertexLabels.keySet()) {
					writer.printf("v %d", vertexId);
					for(String label : vertexLabels.get(vertexId)) {
						writer.printf(" %s", label);
					}
					writer.printf("\n");
				}
				for(int subjId : edges.keySet()) {
					for(int objId : edges.get(subjId).keySet()) {
						writer.printf("e %d %d %s\n", subjId, objId, edges.get(subjId).get(objId));
					}
				}

			}
			writer.flush();


		}

	}
}

