package conceptualKNN_RelEx.utils;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentContainer;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CompareApproaches {
	public static final String DESCRIPTION = "Compare a given approach to C-KNN-RelEx on the true positive examples of this approach";

	public static void addCliArgs(ArgumentContainer parser) {
		parser.description(DESCRIPTION);

		parser.addArgument("approachFile")
				.type(Arguments.fileType().verifyIsFile())
				.help("Path to the output file of the approach");

		parser.addArgument("cknnDir")
				.type(Arguments.fileType().verifyIsDirectory())
				.help("Path to the output dir of C-KNN-RelEx");

		parser.addArgument("outputDir")
				.type(Arguments.fileType().verifyIsDirectory())
				.help("Path to the output directory");
	}

	public static void main(String[] args) throws IOException {
		ArgumentParser parser = ArgumentParsers.newFor(CompareApproaches.class.getName()).build();
		addCliArgs(parser);

		try {
			execute(parser.parseArgs(args));
		} catch(ArgumentParserException e) {
			parser.handleError(e);
			System.exit(1);
		}
	}

	public static void execute(Namespace namespace) throws IOException {
		String approachFile = namespace.getString("approachFile");
		String cknnDir = namespace.getString("cknnDir");
		String outputDir = namespace.getString("outputDir");

		JSONObject approachJson = new JSONObject(new JSONTokener(new FileInputStream(approachFile)));

		Map<String, Integer> predictedByRelation_approach = new HashMap<>();
		Map<String, Integer> correctByRelation_approach = new HashMap<>();
		Map<String, Integer> predictedByRelation_cknn = new HashMap<>();
		Map<String, Integer> correctByRelation_cknn = new HashMap<>();
		Map<String, Integer> goldByRelation = new HashMap<>();
		int correctByApproach = 0;
		int correctByCknn = 0;
		int correctByBoth = 0;
		List<String> correctCknnList = new ArrayList<>();
		List<String> correctApproachList = new ArrayList<>();
		List<String> correctBothList = new ArrayList<>();
		for(String id : approachJson.keySet()) {
			JSONObject result = approachJson.getJSONObject(id);

			if(!result.getString("gold").equals("no_relation") && !result.getString("prediction").equals("no_relation")) {
				String gold = result.getString("gold");
				String approachPred = result.getString("prediction");

				String fileName = cknnDir + "/" + id + ".json";
				JSONObject jsonFile = new JSONObject(new JSONTokener(new FileInputStream(fileName)));
				String cknnPred = jsonFile.getString("guess");

				goldByRelation.put(gold, goldByRelation.getOrDefault(gold, 0) + 1);
				predictedByRelation_approach.put(approachPred, predictedByRelation_approach.getOrDefault(approachPred, 0) + 1);
				if(approachPred.equals(gold)) {
					correctByRelation_approach.put(gold, correctByRelation_approach.getOrDefault(gold, 0) + 1);
				}
				predictedByRelation_cknn.put(cknnPred, predictedByRelation_cknn.getOrDefault(cknnPred, 0) + 1);
				if(cknnPred.equals(gold)) {
					correctByRelation_cknn.put(gold, correctByRelation_cknn.getOrDefault(gold, 0) + 1);
				}

				if(cknnPred.equals(gold) && approachPred.equals(gold)) {
					correctByBoth++;
					correctBothList.add(id);
				} else if(cknnPred.equals(gold)) {
					correctByCknn++;
					correctCknnList.add(id);
				} else if(approachPred.equals(gold)) {
					correctByApproach++;
					correctApproachList.add(id);
				}

			}
		}

		for(String relation : goldByRelation.keySet()) {
			Map<String, Double> statsApproach = AnalyzeResults.stats(
					goldByRelation.getOrDefault(relation, 0),
					correctByRelation_approach.getOrDefault(relation, 0),
					predictedByRelation_approach.getOrDefault(relation, 0)
			);
			Map<String, Double> statsCknn = AnalyzeResults.stats(
					goldByRelation.getOrDefault(relation, 0),
					correctByRelation_cknn.getOrDefault(relation, 0),
					predictedByRelation_cknn.getOrDefault(relation, 0)
			);

			System.out.printf("Relation %s (#: %d):\n", relation, goldByRelation.get(relation));
			System.out.printf("\tApproach:\tP: %.3f\tR: %.3f\tF: %.3f\n",
					statsApproach.get("precision"),
					statsApproach.get("recall"),
					statsApproach.get("f1")
			);
			System.out.printf("\t   C-KNN:\tP: %.3f\tR: %.3f\tF: %.3f\n",
					statsCknn.get("precision"),
					statsCknn.get("recall"),
					statsCknn.get("f1")
			);
		}
		System.out.println();

		int correctTotal_approach = 0;
		for(String relation :
				correctByRelation_approach.keySet()) {
			correctTotal_approach += correctByRelation_approach.get(relation);
		}
		int correctTotal_cknn = 0;
		for(String relation :
				correctByRelation_cknn.keySet()) {
			correctTotal_cknn += correctByRelation_cknn.get(relation);
		}
		int predictedTotal_approach = 0;
		for(String relation : predictedByRelation_approach.keySet()) {
			predictedTotal_approach += predictedByRelation_approach.get(relation);
		}
		int predictedTotal_cknn = 0;
		for(String relation : predictedByRelation_cknn.keySet()) {
			predictedTotal_cknn += predictedByRelation_cknn.get(relation);
		}
		int goldTotal = 0;
		for(String relation : goldByRelation.keySet()) {
			goldTotal += goldByRelation.get(relation);
		}
		Map<String, Double> statsApproach = AnalyzeResults.stats(goldTotal, correctTotal_approach, predictedTotal_approach);
		Map<String, Double> statsCknn = AnalyzeResults.stats(goldTotal, correctTotal_cknn, predictedTotal_cknn);

		System.out.printf("Approach:\t P: %.3f\tR: %.3f\tF: %.3f\n",
				statsApproach.get("precision"),
				statsApproach.get("recall"),
				statsApproach.get("f1")
		);
		System.out.printf("   C-KNN:\t P: %.3f\tR: %.3f\tF: %.3f\n\n",
				statsCknn.get("precision"),
				statsCknn.get("recall"),
				statsCknn.get("f1")
		);

		System.out.printf("Well predicted by both: %.3f\n", (double) correctByBoth / goldTotal);
		System.out.printf("Well predicted by C-KNN: %.3f\n", (double) correctByCknn / goldTotal);
		System.out.printf("Well predicted by approach: %.3f\n", (double) correctByApproach / goldTotal);

		FileWriter approachWriter = new FileWriter(outputDir + "/guessedByApproach.txt");
		for(String id : correctApproachList) {
			approachWriter.write(id + "\n");
		}
		approachWriter.flush();
		approachWriter.close();

		FileWriter cknnWriter = new FileWriter(outputDir + "/guessedByCKNN.txt");
		for(String id : correctCknnList) {
			cknnWriter.write(id + "\n");
		}
		cknnWriter.flush();
		cknnWriter.close();

		FileWriter bothWriter = new FileWriter(outputDir + "/guessedByBoth.txt");
		for(String id : correctBothList) {
			bothWriter.write(id + "\n");
		}
		bothWriter.flush();
		bothWriter.close();
	}
}
