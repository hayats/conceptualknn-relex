package conceptualKNN_RelEx.utils;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.vocabulary.RDFS;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class GenerateOntology {
	public static Model generate() {
		Model model = ModelFactory.createDefaultModel();

		String prefix = "http://example.org/pos#";
		String rdfsPrefix = "http://www.w3.org/2000/01/rdf-schema#";

		model.setNsPrefix("pos", prefix);
		model.setNsPrefix("rdfs", rdfsPrefix);

		Resource jjs = model.createResource(prefix + "JJS");
		Resource jjr = model.createResource(prefix + "JJR");
		Resource jj = model.createResource(prefix + "JJ");
		Resource nns = model.createResource(prefix + "NNS");
		Resource nn = model.createResource(prefix + "NN");
		Resource nnp = model.createResource(prefix + "NNP");
		Resource nnps = model.createResource(prefix + "NNPS");
		Resource rbs = model.createResource(prefix + "RBS");
		Resource rbr = model.createResource(prefix + "RBR");
		Resource rb = model.createResource(prefix + "RB");
		Resource vb = model.createResource(prefix + "VB");
		Resource vbd = model.createResource(prefix + "VBD");
		Resource vbg = model.createResource(prefix + "VBG");
		Resource vbn = model.createResource(prefix + "VBN");
		Resource vbp = model.createResource(prefix + "VBP");
		Resource vbz = model.createResource(prefix + "VBZ");

		Statement stmt = model.createStatement(jjs, RDFS.subClassOf,jjr);
		model.add(stmt);
		stmt = model.createStatement(jjr, RDFS.subClassOf,jj);
		model.add(stmt);
		stmt = model.createStatement(nns, RDFS.subClassOf,nn);
		model.add(stmt);
		stmt = model.createStatement(nnps, RDFS.subClassOf,nnp);
		model.add(stmt);
		stmt = model.createStatement(rbs, RDFS.subClassOf,rbr);
		model.add(stmt);
		stmt = model.createStatement(rbr, RDFS.subClassOf,rb);
		model.add(stmt);
		stmt = model.createStatement(vbd, RDFS.subClassOf,vb);
		model.add(stmt);
		stmt = model.createStatement(vbg, RDFS.subClassOf,vb);
		model.add(stmt);
		stmt = model.createStatement(vbn, RDFS.subClassOf,vb);
		model.add(stmt);
		stmt = model.createStatement(vbp, RDFS.subClassOf,vb);
		model.add(stmt);
		stmt = model.createStatement(vbz, RDFS.subClassOf,vb);
		model.add(stmt);

		return model;
	}
}
