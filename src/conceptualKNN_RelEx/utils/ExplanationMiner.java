package conceptualKNN_RelEx.utils;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentContainer;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

public class ExplanationMiner {
	public static final String DESCRIPTION = "Explanation Miner";

	public static void addCliArgs(ArgumentContainer parser) {
		parser.description(DESCRIPTION);

		parser.addArgument("testFile")
				.type(Arguments.fileType().verifyIsFile())
				.help("path to TACRED test file");

		parser.addArgument("trainFile")
				.type(Arguments.fileType().verifyIsFile())
				.help("path to TACRED train file");

		parser.addArgument("devFile")
				.type(Arguments.fileType().verifyIsFile())
				.help("path to TACRED dev file");

		parser.addArgument("conceptsDirectory")
				.type(Arguments.fileType().verifyIsDirectory())
				.help("path to the result directory");

		parser.addArgument("prettyPrintDirectory")
				.type(Arguments.fileType().verifyIsDirectory())
				.help("path to the directory where to save the prettyPrints");
	}

	public static void main(String[] args) throws IOException {
		ArgumentParser parser = ArgumentParsers.newFor(AnalyzeResults.class.getName()).build();
		addCliArgs(parser);

		try {
			execute(parser.parseArgs(args));
		} catch(ArgumentParserException e) {
			parser.handleError(e);
			System.exit(1);
		}
	}

	public static void execute(Namespace namespace) throws FileNotFoundException {
		JSONArray trainJson = new JSONArray(new JSONTokener(new FileInputStream(namespace.getString("trainFile"))));
		JSONArray devJson = new JSONArray(new JSONTokener(new FileInputStream(namespace.getString("devFile"))));
		JSONArray testJson = new JSONArray(new JSONTokener(new FileInputStream(namespace.getString("testFile"))));

		Map<String, JSONObject> idToSentenceTraining = new HashMap<>();
		for(int i = 0; i < trainJson.length(); i++) {
			JSONObject sentenceJson = trainJson.getJSONObject(i);
			String sentenceId = sentenceJson.getString("id");
			idToSentenceTraining.put(sentenceId, sentenceJson);
		}

		for(int i = 0; i < devJson.length(); i++) {
			JSONObject sentenceJson = devJson.getJSONObject(i);
			String sentenceId = sentenceJson.getString("id");
			idToSentenceTraining.put(sentenceId, sentenceJson);
		}
		Map<String, JSONObject> idToSentenceTest = new HashMap<>();
		for(int i = 0; i < testJson.length(); i++) {
			JSONObject sentenceJson = testJson.getJSONObject(i);
			String sentenceId = sentenceJson.getString("id");
			idToSentenceTest.put(sentenceId, sentenceJson);
		}

		for(String idTest : idToSentenceTest.keySet()) {
			if(new File(namespace.getString("conceptsDirectory") + "/" + idTest + ".json").isFile()) {
				JSONObject result = new JSONObject(
						new JSONTokener(
								new FileInputStream(
										namespace.getString("conceptsDirectory") + "/" + idTest + ".json"
								)
						)
				);

				JSONArray compatRel = result.getJSONArray("compatible_relations");
				String prediction;
				if(compatRel.length() == 1 || result.getString("guess").equals("no_relation")) {
					prediction = result.getString("guess");
				} else {
					prediction = result.getJSONObject("scorers")
							.getJSONArray("maximum_confidence_bis")
							.getJSONObject(0)
							.getString("relation");
				}

				if(!prediction.equals("no_relation") && compatRel.length() > 1) {

					JSONArray score = result.getJSONObject("scorers")
							.getJSONArray("maximum_confidence_bis")
							.getJSONObject(0)
							.getJSONArray("score");

					int nbrConf1 = 0;
					while(score.getJSONObject(nbrConf1).getDouble("score") == 1) {
						nbrConf1++;
					}

					Map<Integer, JSONObject> idToConcept = new HashMap<>();
					JSONArray concepts = result.getJSONArray("conceptsOfNeighbours");
					for(int i = 0; i < concepts.length(); i++) {
						JSONObject concept = concepts.getJSONObject(i);
						int id = concept.getInt("id");
						idToConcept.put(id, concept);
					}

					List<List<String>> intensions = new ArrayList<>();
					for(int i = 0; i < nbrConf1; i++) {
						int id = score.getJSONObject(i).getInt("id");
						JSONObject concept = idToConcept.get(id);

						JSONArray intension = concept.getJSONArray("intension");
						List<String> intensionList = new ArrayList<>();
						for(int j = 0; j < intension.length(); j++) {
							intensionList.add(intension.getString(j));
						}
						intensionList.sort(String::compareTo);
						intensions.add(intensionList);
					}

					List<List<List<String>>> freq_itemsets = apriori(intensions, 0.6);

					System.out.println("ID: " + idTest);
					for(int i = 0; i < freq_itemsets.size(); i++) {
						System.out.printf("Itemset size: %d\n", i + 1);
						List<List<String>> i_itemsets= freq_itemsets.get(i);
						for(int j = 0; j < i_itemsets.size(); j++) {
							System.out.printf("\tItemset %d : \n", j + 1);
							List<String> itemset = i_itemsets.get(j);
							for(String item : itemset) {
								System.out.printf("\t\t%s\n", item);
							}
						}
					}
				}
			}
		}
	}

	private static List<List<List<String>>> apriori(List<List<String>> transactions, double threshold) {
		List<List<List<String>>> freq_itemsets_final = new ArrayList<>();

		// Collecting the set of all items
		Set<String> items = new HashSet<>();
		for(List<String> transaction : transactions) {
			items.addAll(transaction);
		}

		// Creating the candidate 1-itemsets
		List<List<String>> candidates = new ArrayList<>();
		for(String item : items) {
			List<String> itemset = new ArrayList<>();
			itemset.add(item);
			candidates.add(itemset);
		}

		// Selecting the frequent ones
		Map<List<String>, Integer> count = new HashMap<>();
		for(List<String> transac : transactions) {
			for(List<String> candidate : candidates) {
				if(transac.containsAll(candidate)) {
					count.put(candidate, count.getOrDefault(candidate, 0) + 1);
				}
			}
		}
		List<List<String>> freq_itemsets = new ArrayList<>();
		for(List<String> candidate : candidates) {
			if(count.getOrDefault(candidate, 0) / (float) transactions.size() >= threshold) {
				freq_itemsets.add(candidate);
			}
		}
		freq_itemsets_final.add(freq_itemsets);

		int k = 2;
		while(!freq_itemsets.isEmpty()) {
			count = new HashMap<>();
			candidates = gen_candidates(freq_itemsets, k);
			for(List<String> transac : transactions) {
				for(List<String> candidate : candidates) {
					if(transac.containsAll(candidate)) {
						count.put(candidate, count.getOrDefault(candidate, 0) + 1);
					}
				}
			}
			freq_itemsets = new ArrayList<>();
			for(List<String> candidate : candidates) {
				if(count.getOrDefault(candidate, 0) / (float) transactions.size() >= threshold) {
					freq_itemsets.add(candidate);
				}
			}
			if(!freq_itemsets.isEmpty()) {
				freq_itemsets_final.add(freq_itemsets);
			}
			k++;
		}

		return freq_itemsets_final;
	}

	private static List<List<String>> gen_candidates(List<List<String>> freq_prec, int size) {
		List<List<String>> result = new ArrayList<>();
		for(List<String> p : freq_prec) {
			for(List<String> q : freq_prec) {
				boolean similar = true;
				for(int i = 0; i < size - 2; i++) {
					if(!p.get(i).equals(q.get(i))) {
						similar = false;
						break;
					}
				}
				if(similar && p.get(size - 2).compareTo(q.get(size - 2)) > 0) {
					List<String> candidate = new ArrayList<>(p);
					candidate.add(q.get(size - 2));
					candidate.sort(String::compareTo);
					result.add(candidate);
				}
			}
		}
		return result;
	}
}
