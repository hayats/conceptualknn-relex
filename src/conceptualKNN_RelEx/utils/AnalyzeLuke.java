package conceptualKNN_RelEx.utils;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentContainer;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class AnalyzeLuke {
	public static final String DESCRIPTION = "Analyze the results difference between LUKE-ReDect and LUKE-Classif";

	public static void addCliArgs(ArgumentContainer parser) {
		parser.description(DESCRIPTION);

		parser.addArgument("redectFile")
				.type(Arguments.fileType().verifyIsFile())
				.help("path to LUKE-ReDect file");

		parser.addArgument("classifFile")
				.type(Arguments.fileType().verifyIsFile())
				.help("path to LUKE-Classif file");
	}

	public static void main(String[] args) throws FileNotFoundException {
		ArgumentParser parser = ArgumentParsers.newFor(AnalyzeResults.class.getName()).build();
		addCliArgs(parser);

		try {
			execute(parser.parseArgs(args));
		} catch(ArgumentParserException e) {
			parser.handleError(e);
			System.exit(1);
		}
	}

	public static void execute(Namespace namespace) throws FileNotFoundException {
		String redectFile = namespace.getString("redectFile");
		String classifFile = namespace.getString("classifFile");

		JSONObject redectData = new JSONObject(new JSONTokener(new FileInputStream(redectFile)));
		JSONObject classifData = new JSONObject(new JSONTokener(new FileInputStream(classifFile)));

		int bothWellGuessed = 0;
		int redectWellGuessed = 0;
		int classifWellGuessed = 0;
		int total = redectData.length();
		for(String id : redectData.keySet()) {
			String gold = redectData.getJSONObject(id).getString("gold").equals("no_relation") ? "no_relation" : "relation";
			String redectPred = redectData.getJSONObject(id).getString("prediction").equals("no_relation") ? "no_relation" : "relation";
			String classifPred = classifData.getJSONObject(id).getString("prediction").equals("no_relation") ? "no_relation" : "relation";

			if(gold.equals(redectPred) && gold.equals(classifPred)) {
				bothWellGuessed++;
			} else if(gold.equals(redectPred)) {
				redectWellGuessed++;
			} else if(gold.equals(classifPred)) {
				classifWellGuessed++;
			}
		}

		System.out.printf("Nbr of examples: %d\n", total);
		System.out.printf("Nbr of examples well guessed by LUKE_ReDect: %d (%.3f%%)\n",
				bothWellGuessed + redectWellGuessed,
				(float) (bothWellGuessed + redectWellGuessed) * 100 / total);
		System.out.printf("Nbr of examples well guessed by LUKE_Classif: %d (%.3f%%)\n\n",
				bothWellGuessed + classifWellGuessed,
				(float) (bothWellGuessed + classifWellGuessed) * 100 / total);

		System.out.printf("Nbr of examples well guessed by both: %d (%.3f%%)\n",
				bothWellGuessed,
				(float) bothWellGuessed * 100 / total);
		System.out.printf("Nbr of examples well guessed only by LUKE-ReDect :%d (%.3f%%)\n",
				redectWellGuessed,
				(float) redectWellGuessed * 100 / total);
		System.out.printf("Nbr of examples well guessed only by LUKE-Classif :%d (%.3f%%)\n",
				classifWellGuessed,
				(float) classifWellGuessed * 100 / total);
	}
}
