package conceptualKNN_RelEx.utils;

import conceptualKNN_RelEx.RelationExtractionModel;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentContainer;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.vocabulary.RDF;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

public class SparqlQuery {

	public final static String DESCRIPTION = "";

	public static void addCliArgs(ArgumentContainer parser) {
		parser.description(DESCRIPTION);

		parser.addArgument("datasetPath")
				.type(Arguments.fileType().verifyIsDirectory())
				.help("Path to the dataset directory");

		parser.addArgument("wordnetPath")
				.type(Arguments.fileType().verifyIsDirectory())
				.help("Path to WordNet dictionary");

		parser.addArgument("patternFile")
				.type(Arguments.fileType().verifyIsFile())
				.help("Path to the file containing the patterns to process");

	}

	public static void main(String[] args) throws RelExException, IOException {
		ArgumentParser parser = ArgumentParsers.newFor(AnalyzeResults.class.getName()).build();
		addCliArgs(parser);

		try {
			execute(parser.parseArgs(args));
		} catch(ArgumentParserException e) {
			parser.handleError(e);
			System.exit(1);
		}
	}

	public static void execute(Namespace namespace) throws RelExException, IOException {
		String dictPath = namespace.getString("wordnetPath");

		String datasetPath = namespace.getString("datasetPath");

		String patternFile = namespace.getString("patternFile");

		List<String> trainingDatasets = new ArrayList<>();
		trainingDatasets.add(datasetPath + "/train.json");
		trainingDatasets.add(datasetPath + "/dev.json");

		List<String> testDatasets = new ArrayList<>();
		testDatasets.add(datasetPath + "/test.json");

		RelationExtractionModel model = new RelationExtractionModel(true, false, false, 600, dictPath, new ArrayList<>(), false);

		for(String dataset : trainingDatasets) {
			JSONArray jsonDataset = new JSONArray(new JSONTokener(new FileInputStream(dataset)));
			model.importDatabase(jsonDataset, true);
		}

		for(String dataset : testDatasets) {
			JSONArray jsonDataset = new JSONArray(new JSONTokener(new FileInputStream(dataset)));
			model.importDatabase(jsonDataset, false);
		}

		JSONObject object = new JSONObject(new JSONTokener(new FileInputStream(patternFile)));

		JSONArray patterns = object.getJSONArray("patterns");

		for(int i = 0; i < patterns.length(); i++) {
			JSONObject pattern = patterns.getJSONObject(i).getJSONObject("structure");
			JSONArray vertices = pattern.getJSONArray("vertices");
			JSONArray edges = pattern.getJSONArray("edges");

			int root0 = -1;
			int root1 = -1;
			Map<Integer, Set<String>> vertexToLabels = new HashMap<>();
			Map<Integer, Map<Integer, String>> edgesMap = new HashMap<>();

			for(int j = 0; j < vertices.length(); j++) {
				JSONArray labels = vertices.getJSONArray(j);
				vertexToLabels.put(j + 10, new HashSet<>());
				for(int k = 0; k < labels.length(); k++) {
					String label = labels.getString(k);
					if(!label.startsWith("graph:")) {
						vertexToLabels.get(j + 10).add(label);
					}
					if(root0 == -1 && label.startsWith("graph:?Neighbor_0")) {
						root0 = j + 10;
					}
					if(root1 == -1 && label.startsWith("graph:?Neighbor_1")) {
						root1 = j + 10;
					}
				}
			}

			for(int j = 0; j < edges.length(); j++) {
				int subj = edges.getJSONObject(j).getInt("source") + 10;
				String predicate = edges.getJSONObject(j).getString("label");
				int obj = edges.getJSONObject(j).getInt("target") + 10;

				if(!edgesMap.containsKey(subj)) {
					edgesMap.put(subj, new HashMap<>());
				}
				edgesMap.get(subj).put(obj, predicate);
			}


			StringBuilder queryBuilder = new StringBuilder();
			if(root0 != -1 && root1 != -1) {
				queryBuilder.append(String.format("SELECT ?x%d ?x%d\nWHERE {\n", root0, root1));
			} else if(root0 != -1) {
				queryBuilder.append(String.format("SELECT ?x%d \nWHERE {\n", root0));
			} else { // root1 != -1 {
				queryBuilder.append(String.format("SELECT ?x%d \nWHERE {\n", root1));
			}

			for(int vertex : vertexToLabels.keySet()) {
				for(String label : vertexToLabels.get(vertex)) {
					String line = String.format("?x%d <%s> %s .\n", vertex, RDF.type.getURI(), label);
					queryBuilder.append(line);
				}
			}

			for(int subj : edgesMap.keySet()) {
				for(int obj : edgesMap.get(subj).keySet()) {
					String predicate = edgesMap.get(subj).get(obj);
					String line = String.format("?x%d %s ?x%d .\n", subj, predicate, obj);
					queryBuilder.append(line);
				}
			}
			queryBuilder.append("}\n");



			String queryString = queryBuilder.toString();
			if(root0 != -1) {
				String variable = String.format("\\?x%d", root0);
				queryString = queryString.replaceAll(variable, "?Subject");
			}
			if(root1 != -1) {
				String variable = String.format("\\?x%d", root1);
				queryString = queryString.replaceAll(variable, "?Object");
			}

			System.out.println(queryString);

			Query query = QueryFactory.create(queryString);

			Set<String> results = model.executeQuery(query);

			for(String sentenceId : results) {
				System.out.println(sentenceId);
			}
		}

	}
}
