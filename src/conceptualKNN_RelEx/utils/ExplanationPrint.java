package conceptualKNN_RelEx.utils;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentContainer;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExplanationPrint {

	public static final String DESCRIPTION = "Pretty-print of explanation";

	public static void addCliArgs(ArgumentContainer parser) {
		parser.description(DESCRIPTION);

		parser.addArgument("testFile")
				.type(Arguments.fileType().verifyIsFile())
				.help("path to TACRED test file");

		parser.addArgument("trainFile")
				.type(Arguments.fileType().verifyIsFile())
				.help("path to TACRED train file");

		parser.addArgument("devFile")
				.type(Arguments.fileType().verifyIsFile())
				.help("path to TACRED dev file");

		parser.addArgument("conceptsDirectory")
				.type(Arguments.fileType().verifyIsDirectory())
				.help("path to the result directory");

		parser.addArgument("prettyPrintDirectory")
				.type(Arguments.fileType().verifyIsDirectory())
				.help("path to the directory where to save the prettyPrints");
	}

	public static void main(String[] args) throws IOException {
		ArgumentParser parser = ArgumentParsers.newFor(AnalyzeResults.class.getName()).build();
		addCliArgs(parser);

		try {
			execute(parser.parseArgs(args));
		} catch(ArgumentParserException e) {
			parser.handleError(e);
			System.exit(1);
		}
	}

	public static void execute(Namespace namespace) throws IOException {
		JSONArray trainJson = new JSONArray(new JSONTokener(new FileInputStream(namespace.getString("trainFile"))));
		JSONArray devJson = new JSONArray(new JSONTokener(new FileInputStream(namespace.getString("devFile"))));
		JSONArray testJson = new JSONArray(new JSONTokener(new FileInputStream(namespace.getString("testFile"))));
		System.out.println("Files loaded");

		Map<String, JSONObject> idToSentenceTraining = new HashMap<>();
		for(int i = 0; i < trainJson.length(); i++) {
			JSONObject sentenceJson = trainJson.getJSONObject(i);
			String sentenceId = sentenceJson.getString("id");
			idToSentenceTraining.put(sentenceId, sentenceJson);
		}

		for(int i = 0; i < devJson.length(); i++) {
			JSONObject sentenceJson = devJson.getJSONObject(i);
			String sentenceId = sentenceJson.getString("id");
			idToSentenceTraining.put(sentenceId, sentenceJson);
		}
		Map<String, JSONObject> idToSentenceTest = new HashMap<>();
		for(int i = 0; i < testJson.length(); i++) {
			JSONObject sentenceJson = testJson.getJSONObject(i);
			String sentenceId = sentenceJson.getString("id");
			idToSentenceTest.put(sentenceId, sentenceJson);
		}

		for(String idTest : idToSentenceTest.keySet()) {
			if(new File(namespace.getString("conceptsDirectory") + "/" + idTest + ".json").isFile()) {
				JSONObject sentence = idToSentenceTest.get(idTest);
				JSONObject result = new JSONObject(
						new JSONTokener(
								new FileInputStream(
										namespace.getString("conceptsDirectory") + "/" + idTest + ".json"
								)
						)
				);

				FileWriter writer = new FileWriter(namespace.getString("prettyPrintDirectory") + "/" + idTest + ".txt");

				writer.write("Sentence ID: " + idTest + "\n");

				writer.write("Sentence: \"");
				JSONArray tokens = sentence.getJSONArray("token");
				for(int i = 0; i < tokens.length(); i++) {
					writer.write(tokens.getString(i) + " ");
				}
				writer.write("\"\n\n");

				int subjBegin = sentence.getInt("subj_start");
				int subjEnd = sentence.getInt("subj_end");
				writer.write("Subject: \"");
				for(int i = subjBegin; i <= subjEnd; i++) {
					writer.write(tokens.getString(i) + " ");
				}
				writer.write("\"\n\n");

				int objBegin = sentence.getInt("obj_start");
				int objEnd = sentence.getInt("obj_end");
				writer.write("Object: \"");
				for(int i = objBegin; i <= objEnd; i++) {
					writer.write(tokens.getString(i) + " ");
				}
				writer.write("\"\n\n");

				writer.write("Compatible relations:\n");
				JSONArray compatRel = result.getJSONArray("compatible_relations");
				for(int i = 0; i < compatRel.length(); i++) {
					writer.write("\t" + compatRel.getString(i) + "\n");
				}
				writer.write("\n");

				writer.write("Prediction: ");
				String prediction;
				if(compatRel.length() == 1 || result.getString("guess").equals("no_relation")) {
					prediction = result.getString("guess");
				} else {
					prediction = result.getJSONObject("scorers")
							.getJSONArray("maximum_confidence_bis")
							.getJSONObject(0)
							.getString("relation");
				}
				writer.write(prediction + "\n\n");

				writer.write("Gold: ");
				String gold = result.getString("gold");
				writer.write(gold + "\n\n");

				if(!prediction.equals("no_relation") && compatRel.length() > 1) {
					writer.write("Explanation:\n");

					JSONArray score = result.getJSONObject("scorers")
							.getJSONArray("maximum_confidence_bis")
							.getJSONObject(0)
							.getJSONArray("score");

					JSONArray score2 = result.getJSONObject("scorers")
							.getJSONArray("maximum_confidence_bis")
							.getJSONObject(1)
							.getJSONArray("score");

					int nbrConf1 = 0;
					while(score.getJSONObject(nbrConf1).getDouble("score") == 1) {
						nbrConf1++;
					}
					writer.write("\tNb of rules predicting the prediction w/ a conf. of 1: " + nbrConf1 + "\n");

					int nbrConf1_2 = 0;
					while(score2.getJSONObject(nbrConf1_2).getDouble("score") == 1) {
						nbrConf1_2++;
					}
					writer.write("\tNb of rules predicting the second candidate w/ a conf. of 1: " + nbrConf1_2 + "\n\n");

					writer.write("\tRules of confidence 1:\n");
					Map<Integer, JSONObject> idToConcept = new HashMap<>();
					JSONArray concepts = result.getJSONArray("conceptsOfNeighbours");
					for(int i = 0; i < concepts.length(); i++) {
						JSONObject concept = concepts.getJSONObject(i);
						int id = concept.getInt("id");
						idToConcept.put(id, concept);
					}

					for(int i = 0; i < nbrConf1; i++) {
						int id = score.getJSONObject(i).getInt("id");
						JSONObject concept = idToConcept.get(id);

						writer.write("\t\tIntension: \n");
						JSONArray intension = concept.getJSONArray("intension");
						List<String> sortedIntension = new ArrayList<>();
						for(int j = 0; j < intension.length(); j++) {
							sortedIntension.add(intension.getString(j));
						}
						sortedIntension.sort(String::compareTo);
						for(String elt : sortedIntension) {
							writer.write("\t\t\t" + elt + "\n");
						}
						writer.write("\t\tExtension:\n");
						JSONArray propExt = concept.getJSONArray("extension");
						for(int j = 0; j < propExt.length(); j++) {
							String idSentencePropExt = propExt.getJSONArray(j).getString(0)
									.replaceAll("^.*#", "")
									.replaceAll("_.*$", "");
							JSONObject sentencePropExt = idToSentenceTraining.get(idSentencePropExt);
							writer.write("\t\t\tSentence: \"");
							JSONArray tokensProperExt = sentencePropExt.getJSONArray("token");
							for(int k = 0; k < tokensProperExt.length(); k++) {
								writer.write(tokensProperExt.getString(k) + " ");
							}
							writer.write("\"\n");

							subjBegin = sentencePropExt.getInt("subj_start");
							subjEnd = sentencePropExt.getInt("subj_end");
							writer.write("\t\t\tSubject: \"");
							for(int k = subjBegin; k <= subjEnd; k++) {
								writer.write(tokensProperExt.getString(k) + " ");
							}
							writer.write("\"\n");

							objBegin = sentencePropExt.getInt("obj_start");
							objEnd = sentencePropExt.getInt("obj_end");
							writer.write("\t\t\tObject: \"");
							for(int k = objBegin; k <= objEnd; k++) {
								writer.write(tokensProperExt.getString(k) + " ");
							}
							writer.write("\"\n\n");
						}
						writer.write("\n");
					}
				}


				writer.flush();
				writer.close();
			}
		}
	}
}
