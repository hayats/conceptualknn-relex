package conceptualKNN_RelEx.utils;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentContainer;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class LukeToTacred {
	public static final String DESCRIPTION = "Add to TACRED dataset the predictions made by LUKE-ReDetect";

	public static void addCliArgs(ArgumentContainer parser) {
		parser.description(DESCRIPTION);

		parser.addArgument("lukeFile")
				.type(Arguments.fileType().verifyIsFile())
				.help("Path to the LUKE-ReDetect file");

		parser.addArgument("tacredFile")
				.type(Arguments.fileType().verifyIsFile())
				.help("Path to the TACRED test file");

		parser.addArgument("outputFile")
				.type(Arguments.fileType().verifyCanCreate())
				.help("Path to the output file");

		parser.addArgument("-v")
				.action(Arguments.storeTrue())
				.dest("verbose")
				.help("Verbose flag (disabled by default)");
	}

	public static void main(String[] args) throws IOException {
		ArgumentParser parser = ArgumentParsers.newFor(LukeToTacred.class.getName()).build();
		addCliArgs(parser);

		try {
			execute(parser.parseArgs(args));
		} catch(ArgumentParserException e) {
			parser.handleError(e);
			System.exit(1);
		}
	}

	public static void execute(Namespace namespace) throws IOException {
		String lukeFile = namespace.getString("lukeFile");
		String tacredFile = namespace.getString("tacredFile");
		String outputFile = namespace.getString("outputFile");
		boolean verbose = namespace.getBoolean("verbose");

		if(verbose) {
			System.out.println("Loading files...");
		}
		JSONObject lukeJson = new JSONObject(new JSONTokener(new FileInputStream(lukeFile)));
		JSONArray tacredJson = new JSONArray(new JSONTokener(new FileInputStream(tacredFile)));

		if(verbose) {
			System.out.println("Processing examples...");
		}
		for(int i = 0; i < tacredJson.length(); i++) {
			JSONObject example = tacredJson.getJSONObject(i);
			String id = example.getString("id");
			if(verbose) {
				System.out.printf("\t[%d/%d] Processing example %s\n", i + 1, tacredJson.length(), id);
			}
			String lukePrediction = lukeJson.getJSONObject(id).getString("prediction");
			example.put("luke_annotation", lukePrediction);
		}

		if(verbose) {
			System.out.println("Writing output file...");
		}

		Writer writer = new FileWriter(outputFile);
		tacredJson.write(writer, 4, 0);
		writer.flush();
		writer.close();
	}
}
