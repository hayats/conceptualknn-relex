package conceptualKNN_RelEx.utils;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

public class PatternSelection {

	public static void main(String[] args) throws IOException {
		String inputFile = args[0];

		JSONObject file = new JSONObject(new JSONTokener(new FileInputStream(inputFile)));
		JSONArray patterns = file.getJSONArray("patterns");

		JSONArray selectedPatterns = new JSONArray();

		for(int i = 0; i < patterns.length(); i++) {
			boolean containsNeighbour = false;
			boolean containsLemma = false;

			JSONObject pattern = patterns.getJSONObject(i);
			JSONArray vertices = pattern.getJSONObject("structure").getJSONArray("vertices");
			for(int j = 0; j < vertices.length(); j++) {
				JSONArray vertice = vertices.getJSONArray(j);
				for(int k = 0; k < vertice.length(); k++) {
					String label = vertice.getString(k);
					if(label.equals("graph:?Neighbor_0") || label.equals("graph:?Neighbor_1")) {
						containsNeighbour = true;
					}
					if(label.contains("lemma#") || label.contains("synset#")) {
						containsLemma = true;
					}
				}
			}
			if(containsNeighbour && containsLemma) {
				selectedPatterns.put(pattern);
			}
		}

		file.put("patterns", selectedPatterns);

		String outputFile = args[1];
		FileWriter writer = new FileWriter(outputFile);
		file.write(writer);
		writer.flush();
		writer.close();
	}
}
