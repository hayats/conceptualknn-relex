package conceptualKNN_RelEx.utils;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentContainer;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class AnalyzeResults {
	public final static String DESCRIPTION = "Analyze the results of a full experiment";

	public static void addCliArgs(ArgumentContainer parser) {
		parser.description(DESCRIPTION);

		parser.addArgument("resultsPath")
				.type(Arguments.fileType().verifyIsDirectory())
				.help("Path to the output directory of the experiment to analyse");

		parser.addArgument("-v")
				.action(Arguments.storeTrue())
				.dest("verbose")
				.help("Verbose flag (disabled by default)");

	}

	public static void main(String[] args) throws FileNotFoundException {
		ArgumentParser parser = ArgumentParsers.newFor(AnalyzeResults.class.getName()).build();
		addCliArgs(parser);

		try {
			Namespace argNamespace = parser.parseArgs(args);
			execute(argNamespace);
		} catch(ArgumentParserException e) {
			parser.handleError(e);
			System.exit(1);
		}
	}

	public static void execute(Namespace namespace) throws FileNotFoundException {
		String dirPath = namespace.getString("resultsPath");
		boolean verbose = namespace.getBoolean("verbose");
		run(dirPath, verbose);
	}

	public static void run(String dirPath, boolean verbose) throws FileNotFoundException {
		File dir = new File(dirPath);

		String[] files = dir.list();

		int nbDifficultExamples = 0;
		int nbTrivialExamples = 0;
		int nbWellGuessedTrivial = 0;
		int nbTotalConcepts = 0;
		int totalMaxIntensionSize = 0;
		int wellGuessedExpoVote = 0;
		int wellGuessedMaxConf = 0;
		int wellGuessedMaxConfBis = 0;
		int fullyProcessed = 0;

		Map<String, Integer> predictedByRelation_expoVote = new HashMap<>();
		Map<String, Integer> predictedByRelation_maxConf = new HashMap<>();
		Map<String, Integer> predictedByRelation_maxConfBis = new HashMap<>();
		Map<String, Integer> correctByRelation_expoVote = new HashMap<>();
		Map<String, Integer> correctByRelation_maxConf = new HashMap<>();
		Map<String, Integer> correctByRelation_maxConfBis = new HashMap<>();
		Map<String, Integer> goldByRelation = new HashMap<>();

		assert files != null;
		for(String file : files) {
			if(verbose) {
				System.out.printf("Processing file %s...\n", file);
			}
			String path = dirPath + "/" + file;

			JSONObject result = new JSONObject(new JSONTokener(new FileInputStream(path)));

			String expoVotePrediction;
			String maxConfPrediction;
			String maxConfBisPrediction;
			if(result.getJSONArray("compatible_relations").length() > 1) {
				JSONObject scorers = result.getJSONObject("scorers");
				expoVotePrediction = scorers.getJSONArray("exponential_vote").getJSONObject(0).getString("relation");
				maxConfPrediction = scorers.getJSONArray("maximum_confidence").getJSONObject(0).getString("relation");
				maxConfBisPrediction = scorers.getJSONArray("maximum_confidence_bis").getJSONObject(0).getString("relation");
			} else {
				expoVotePrediction = result.getString("guess");
				maxConfPrediction = result.getString("guess");
				maxConfBisPrediction = result.getString("guess");
			}
			String gold = result.getString("gold");

			if(result.getJSONArray("compatible_relations").length() > 1) {
				nbDifficultExamples++;

				if(result.has("fully_processed") && result.getBoolean("fully_processed")) {
					fullyProcessed++;
				}

				JSONArray concepts = result.getJSONArray("conceptsOfNeighbours");
				nbTotalConcepts += concepts.length();

				int maxIntensionSize = 0;
				for(int i = 0; i < concepts.length(); i++) {
					JSONObject concept = concepts.getJSONObject(i);
					if(concept.getJSONArray("intension").length() > maxIntensionSize) {
						maxIntensionSize = concept.getJSONArray("intension").length();
					}
				}
				totalMaxIntensionSize += maxIntensionSize;

				if(maxConfPrediction.equals(result.getString("gold"))) {
					wellGuessedMaxConf++;
				}

				if(maxConfBisPrediction.equals(result.getString("gold"))) {
					wellGuessedMaxConfBis++;
				}

				if(expoVotePrediction.equals(result.getString("gold"))) {
					wellGuessedExpoVote++;
				}

			} else {
				nbTrivialExamples++;

				if(result.getString("guess").equals(result.getString("gold"))) {
					nbWellGuessedTrivial++;
				}
			}

			if(gold.equals("no_relation") && !expoVotePrediction.equals("no_relation")) {
				predictedByRelation_expoVote.put(expoVotePrediction, predictedByRelation_expoVote.getOrDefault(expoVotePrediction, 0) + 1);
			} else if(!gold.equals("no_relation") && expoVotePrediction.equals("no_relation")) {
				goldByRelation.put(gold, goldByRelation.getOrDefault(gold, 0) + 1);
			} else if(!gold.equals("no_relation")) {
				predictedByRelation_expoVote.put(expoVotePrediction, predictedByRelation_expoVote.getOrDefault(expoVotePrediction, 0) + 1);
				goldByRelation.put(gold, goldByRelation.getOrDefault(gold, 0) + 1);
				if(gold.equals(expoVotePrediction)) {
					correctByRelation_expoVote.put(gold, correctByRelation_expoVote.getOrDefault(gold, 0) + 1);
				}
			}

			if(gold.equals("no_relation") && !maxConfPrediction.equals("no_relation")) {
				predictedByRelation_maxConf.put(maxConfPrediction, predictedByRelation_maxConf.getOrDefault(maxConfPrediction, 0) + 1);
			} else if(!gold.equals("no_relation") && !maxConfPrediction.equals("no_relation")) {
				predictedByRelation_maxConf.put(maxConfPrediction, predictedByRelation_maxConf.getOrDefault(maxConfPrediction, 0) + 1);
				if(gold.equals(maxConfPrediction)) {
					correctByRelation_maxConf.put(gold, correctByRelation_maxConf.getOrDefault(gold, 0) + 1);
				}
			}

			if(gold.equals("no_relation") && !maxConfBisPrediction.equals("no_relation")) {
				predictedByRelation_maxConfBis.put(maxConfBisPrediction, predictedByRelation_maxConfBis.getOrDefault(maxConfBisPrediction, 0) + 1);
			} else if(!gold.equals("no_relation") && !maxConfBisPrediction.equals("no_relation")) {
				predictedByRelation_maxConfBis.put(maxConfBisPrediction, predictedByRelation_maxConfBis.getOrDefault(maxConfBisPrediction, 0) + 1);
				if(gold.equals(maxConfBisPrediction)) {
					correctByRelation_maxConfBis.put(gold, correctByRelation_maxConfBis.getOrDefault(gold, 0) + 1);
				}
			}
		}

		Set<String> relations = new HashSet<>();
		relations.addAll(goldByRelation.keySet());
		relations.addAll(predictedByRelation_expoVote.keySet());
		relations.addAll(predictedByRelation_maxConf.keySet());
		relations.addAll(correctByRelation_expoVote.keySet());
		relations.addAll(correctByRelation_maxConf.keySet());

		System.out.printf("Number of examples: %d\n\n", files.length);

		System.out.printf("Number of trivial examples: %d (%.3f%%)\n", nbTrivialExamples, 100 * ((float) nbTrivialExamples) / files.length);
		System.out.printf("\tNumber of well guessed: %d (%.3f%%)\n\n", nbWellGuessedTrivial, 100 * ((float) nbWellGuessedTrivial) / nbTrivialExamples);

		System.out.printf("Number of difficult examples: %d (%f%%)\n", nbDifficultExamples, 100 * ((float) nbDifficultExamples) / files.length);
		System.out.printf("\tAvg number of concepts by partition: %f\n", ((float) nbTotalConcepts) / nbDifficultExamples);
		System.out.printf("\tAvg max intension size per concept: %f\n", ((float) totalMaxIntensionSize) / nbDifficultExamples);
		System.out.printf("\tNb of examples fully processed: %d (%f%%)\n\n", fullyProcessed, 100 * ((float) fullyProcessed) / nbDifficultExamples);

		System.out.printf("Number of examples well guessed (Maximum Confidence): %d (%.3f%%)\n", wellGuessedMaxConf + nbWellGuessedTrivial, 100 * ((float) wellGuessedMaxConf + nbWellGuessedTrivial) / files.length);
		System.out.printf("Number of examples well guessed (Maximum Confidence Bis): %d (%.3f%%)\n", wellGuessedMaxConfBis + nbWellGuessedTrivial, 100 * ((float) wellGuessedMaxConfBis + nbWellGuessedTrivial) / files.length);
		System.out.printf("Number of examples well guessed (Exponential Vote): %d (%.3f%%)\n\n", wellGuessedExpoVote + nbWellGuessedTrivial, 100 * ((float) wellGuessedExpoVote + nbWellGuessedTrivial) / files.length);

		System.out.println("Maximum Confidence:");
		System.out.println("\tPer-relation statistics:");
		for(String relation : relations) {
			int correct = correctByRelation_maxConf.getOrDefault(relation, 0);
			int predicted = predictedByRelation_maxConf.getOrDefault(relation, 0);
			int gold = goldByRelation.getOrDefault(relation, 0);

			Map<String, Double> stats = stats(gold,correct,predicted);
			System.out.printf("\t\t%s:\tP = %.3f\tR = %.3f\tF1 = %.3f\t#: %d\n", relation, stats.get("precision")*100, stats.get("recall")*100, stats.get("f1")*100, gold);
		}

		int goldTotal = 0;
		int correctTotal_maxConf = 0;
		int predictedTotal_maxConf = 0;
		for(String relation : relations) {
			goldTotal += goldByRelation.getOrDefault(relation, 0);
			correctTotal_maxConf += correctByRelation_maxConf.getOrDefault(relation, 0);
			predictedTotal_maxConf += predictedByRelation_maxConf.getOrDefault(relation, 0);
		}
		Map<String, Double> stats_maxConf = stats(goldTotal, correctTotal_maxConf, predictedTotal_maxConf);

		System.out.printf("\tGlobal statistics:\n\t\tP = %.3f\n\t\tR = %.3f\n\t\tF1 = %.3f\n\n", stats_maxConf.get("precision")*100, stats_maxConf.get("recall")*100, stats_maxConf.get("f1")*100);

		System.out.println("Maximum Confidence Bis:");
		System.out.println("\tPer-relation statistics:");
		for(String relation : relations) {
			int correct = correctByRelation_maxConfBis.getOrDefault(relation, 0);
			int predicted = predictedByRelation_maxConfBis.getOrDefault(relation, 0);
			int gold = goldByRelation.getOrDefault(relation, 0);

			Map<String, Double> stats = stats(gold,correct,predicted);
			System.out.printf("\t\t%s:\tP = %.3f\tR = %.3f\tF1 = %.3f\t#: %d\n", relation, stats.get("precision")*100, stats.get("recall")*100, stats.get("f1")*100, gold);
		}

		int correctTotal_maxConfBis = 0;
		int predictedTotal_maxConfBis = 0;
		for(String relation : relations) {
			correctTotal_maxConfBis += correctByRelation_maxConfBis.getOrDefault(relation, 0);
			predictedTotal_maxConfBis += predictedByRelation_maxConfBis.getOrDefault(relation, 0);
		}
		Map<String, Double> stats_maxConfBis = stats(goldTotal, correctTotal_maxConfBis, predictedTotal_maxConfBis);

		System.out.printf("\tGlobal statistics:\n\t\tP = %.3f\n\t\tR = %.3f\n\t\tF1 = %.3f\n\n", stats_maxConfBis.get("precision")*100, stats_maxConfBis.get("recall")*100, stats_maxConfBis.get("f1")*100);

		System.out.println("ExponentialVote:");
		System.out.println("\tPer-relation statistics:");
		for(String relation : relations) {
			int correct = correctByRelation_expoVote.getOrDefault(relation, 0);
			int predicted = predictedByRelation_expoVote.getOrDefault(relation, 0);
			int gold = goldByRelation.getOrDefault(relation, 0);

			Map<String, Double> stats = stats(gold,correct,predicted);
			System.out.printf("\t\t%s:\tP = %.3f\tR = %.3f\tF1 = %.3f\t#: %d\n", relation, stats.get("precision")*100, stats.get("recall")*100, stats.get("f1")*100, gold);
		}

		int correctTotal_expoVote = 0;
		int predictedTotal_expoVote = 0;
		for(String relation : relations) {
			correctTotal_expoVote += correctByRelation_expoVote.getOrDefault(relation, 0);
			predictedTotal_expoVote += predictedByRelation_expoVote.getOrDefault(relation, 0);
		}
		Map<String, Double> stats_expoVote = stats(goldTotal, correctTotal_expoVote, predictedTotal_expoVote);

		System.out.printf("\tGlobal statistics:\n\t\tP = %.3f\n\t\tR = %.3f\n\t\tF1 = %.3f\n\n", stats_expoVote.get("precision")*100, stats_expoVote.get("recall")*100, stats_expoVote.get("f1")*100);
	}

	public static Map<String, Double> stats(int gold, int correct, int predicted) {
		Map<String, Double> returnValue = new HashMap<>();
		double precision = 1;
		if(predicted > 0) {
			precision = (double) correct / predicted;
		}
		double recall = 0;
		if(gold > 0) {
			recall = (double) correct / gold;
		}
		double f1 = 0;
		if(precision + recall > 0) {
			f1 = 2 * precision * recall / (precision + recall);
		}
		returnValue.put("precision", precision);
		returnValue.put("recall", recall);
		returnValue.put("f1",f1);
		return returnValue;
	}
}
