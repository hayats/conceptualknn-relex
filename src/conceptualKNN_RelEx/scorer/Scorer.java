package conceptualKNN_RelEx.scorer;

import conceptualKNN.Partition;
import conceptualKNN_RelEx.RelationExtractionModel;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.*;

public abstract class Scorer {
	public abstract JSONArray score(RelationExtractionModel model, Partition partition);

	public abstract String toString();

	static class ExponentialVote extends Scorer {
		private Map<String, Double> scoring(RelationExtractionModel model, Partition partition) {
			double maxArgValue = -Math.log(Double.MIN_VALUE) / 2;
			int maxDistanceValue = model.getTrainingPairList().size();
			JSONObject partitionJson = new JSONObject(new JSONTokener(partition.toJson()));

			Map<String, Double> scores = new HashMap<>();
			JSONArray clusters = partitionJson.getJSONArray("conceptsOfNeighbours");

			int nbClusters = clusters.length();

			for(int i = 0; i < nbClusters; i++) {
				JSONObject cluster = clusters.getJSONObject(i);
				int extensionalDistance = cluster.getInt("extensionalDistance");

				JSONArray answers = cluster.getJSONArray("answers");
				for(int j = 0; j < answers.length(); j++) {
					double score = Math.exp(-(((double) extensionalDistance) / maxDistanceValue) * maxArgValue);
					JSONArray ans = answers.getJSONArray(j);
					String pairId = ans.getString(0).replaceAll(".*#", "").replaceAll("_.*", "");
					String relation = model.getTrainingPair(pairId).getRelation();
					if(scores.containsKey(relation)) {
						double prev = scores.get(relation);
						scores.put(relation, prev + score);
					} else {
						scores.put(relation, score);
					}
				}
			}

			return scores;
		}

		@Override
		public JSONArray score(RelationExtractionModel model, Partition partition) {
			Map<String, Double> scores = this.scoring(model, partition);

			List<String> sortedRelations = this.sort(scores);

			JSONArray jsonScores = new JSONArray();

			for(String relation : sortedRelations) {
				JSONObject relationScore = new JSONObject();
				relationScore.put("relation", relation);
				relationScore.put("score", scores.get(relation));
				jsonScores.put(relationScore);
			}

			return jsonScores;
		}

		@Override
		public String toString() {
			return "exponential_vote";
		}

		private List<String> sort(Map<String, Double> scores) {
			List<String> sorted = new ArrayList<>(scores.keySet());
			sorted.sort((rel1, rel2) -> -Double.compare(scores.get(rel1), scores.get(rel2)));

			return sorted;
		}
	}

	static class MaximumConfidence extends Scorer {
		private Map<String, List<PrivateScore>> scoring(RelationExtractionModel model, Partition partition) {
			Map<String, List<PrivateScore>> scores = new HashMap<>();

			JSONObject partitionJSON = new JSONObject(new JSONTokener(partition.toJson()));
			JSONArray clusters = partitionJSON.getJSONArray("conceptsOfNeighbours");

			int nbClusters = clusters.length();

			for(int i = 0; i < nbClusters; i++) {
				JSONObject cluster = clusters.getJSONObject(i);
				int id = cluster.getInt("id");
				int extensionalDistance = cluster.getInt("extensionalDistance");

				JSONArray answers = cluster.getJSONArray("answers");
				Map<String, Integer> countByRelation = new HashMap<>();

				for(int j = 0; j < answers.length(); j++) {
					JSONArray ans = answers.getJSONArray(j);
					String pairId = ans.getString(0).replaceAll(".*#", "").replaceAll("_.*", "");
					String relation = model.getTrainingPair(pairId).getRelation();
					if(countByRelation.containsKey(relation)) {
						countByRelation.put(relation, countByRelation.get(relation) + 1);
					} else {
						countByRelation.put(relation, 1);
					}
				}

				countByRelation.forEach((rel, count) -> {
					if(!scores.containsKey(rel)) {
						scores.put(rel, new ArrayList<>());
					}
					PrivateScore score = new PrivateScore();
					score.id = id;
					score.score = ((double) countByRelation.get(rel)) / (extensionalDistance);
					scores.get(rel).add(score);
				});

				for(String rel : scores.keySet()) {
					scores.get(rel).sort((s1, s2) -> -Double.compare(s1.score, s2.score));
				}
			}

			return scores;
		}

		@Override
		public JSONArray score(RelationExtractionModel model, Partition partition) {
			Map<String, List<PrivateScore>> scores = this.scoring(model, partition);

			List<String> sortedRel = this.sort(scores);

			JSONArray jsonScores = new JSONArray();

			for(String rel : sortedRel) {
				JSONObject score = new JSONObject();
				score.put("relation", rel);
				JSONArray detailedScores = new JSONArray();
				for(PrivateScore privateScore : scores.get(rel)) {
					JSONObject detailedScore = new JSONObject();
					detailedScore.put("id", privateScore.id);
					detailedScore.put("score", privateScore.score);
					detailedScores.put(detailedScore);
				}
				score.put("score", detailedScores);
				jsonScores.put(score);
			}

			return jsonScores;
		}

		@Override
		public String toString() {
			return "maximum_confidence";
		}

		private List<String> sort(Map<String, List<PrivateScore>> scores) {
			List<String> sorted = new ArrayList<>(scores.keySet());

			sorted.sort((rel1, rel2) -> -this.comparatorAux(scores.get(rel1), scores.get(rel2), 0));

			return sorted;
		}

		private int comparatorAux(List<PrivateScore> scores1, List<PrivateScore> scores2, int index) {
			if(scores1.size() <= index && scores2.size() <= index) {
				return 0;
			} else if(scores1.size() <= index) {
				return 1;
			} else if(scores2.size() <= index) {
				return -1;
			} else if(scores1.get(index).score == scores2.get(index).score) {
				return comparatorAux(scores1, scores2, index + 1);
			} else {
				return (scores1.get(index).score < scores2.get(index).score ? -1 : 1);
			}

		}

		private static class PrivateScore {
			public int id;
			public double score;
		}
	}

	static class MaximumConfidenceBis extends Scorer {
		private Map<String, List<PrivateScore>> scoring(RelationExtractionModel model, Partition partition) {
			Map<String, List<PrivateScore>> scores = new HashMap<>();

			JSONObject partitionJSON = new JSONObject(new JSONTokener(partition.toJson()));
			JSONArray clusters = partitionJSON.getJSONArray("conceptsOfNeighbours");

			int nbClusters = clusters.length();

			for(int i = 0; i < nbClusters; i++) {
				JSONObject cluster = clusters.getJSONObject(i);
				int id = cluster.getInt("id");
				int extensionalDistance = cluster.getInt("extensionalDistance");

				JSONArray answers = cluster.getJSONArray("extension");
				Map<String, Integer> countByRelation = new HashMap<>();

				for(int j = 0; j < answers.length(); j++) {
					JSONArray ans = answers.getJSONArray(j);
					String pairId = ans.getString(0).replaceAll(".*#", "").replaceAll("_.*", "");
					String relation = model.getTrainingPair(pairId).getRelation();
					if(countByRelation.containsKey(relation)) {
						countByRelation.put(relation, countByRelation.get(relation) + 1);
					} else {
						countByRelation.put(relation, 1);
					}
				}

				countByRelation.forEach((rel, count) -> {
					if(!scores.containsKey(rel)) {
						scores.put(rel, new ArrayList<>());
					}
					PrivateScore score = new PrivateScore();
					score.id = id;
					score.score = ((double) countByRelation.get(rel)) / (extensionalDistance);
					scores.get(rel).add(score);
				});

				for(String rel : scores.keySet()) {
					scores.get(rel).sort((s1, s2) -> -Double.compare(s1.score, s2.score));
				}
			}

			return scores;
		}

		@Override
		public JSONArray score(RelationExtractionModel model, Partition partition) {
			Map<String, List<PrivateScore>> scores = this.scoring(model, partition);

			List<String> sortedRel = this.sort(scores);

			JSONArray jsonScores = new JSONArray();

			for(String rel : sortedRel) {
				JSONObject score = new JSONObject();
				score.put("relation", rel);
				JSONArray detailedScores = new JSONArray();
				for(PrivateScore privateScore : scores.get(rel)) {
					JSONObject detailedScore = new JSONObject();
					detailedScore.put("id", privateScore.id);
					detailedScore.put("score", privateScore.score);
					detailedScores.put(detailedScore);
				}
				score.put("score", detailedScores);
				jsonScores.put(score);
			}

			return jsonScores;
		}

		@Override
		public String toString() {
			return "maximum_confidence_bis";
		}

		private List<String> sort(Map<String, List<PrivateScore>> scores) {
			List<String> sorted = new ArrayList<>(scores.keySet());

			sorted.sort((rel1, rel2) -> -this.comparatorAux(scores.get(rel1), scores.get(rel2), 0));

			return sorted;
		}

		private int comparatorAux(List<PrivateScore> scores1, List<PrivateScore> scores2, int index) {
			if(scores1.size() <= index && scores2.size() <= index) {
				return 0;
			} else if(scores1.size() <= index) {
				return 1;
			} else if(scores2.size() <= index) {
				return -1;
			} else if(scores1.get(index).score == scores2.get(index).score) {
				return comparatorAux(scores1, scores2, index + 1);
			} else {
				return (scores1.get(index).score < scores2.get(index).score ? -1 : 1);
			}

		}

		private static class PrivateScore {
			public int id;
			public double score;
		}
	}

	static class MaximumRectifiedConfidence extends Scorer {
		private static final int LAMBDA = 1;

		private Map<String, List<PrivateScore>> scoring(RelationExtractionModel model, Partition partition) {
			Map<String, List<PrivateScore>> scores = new HashMap<>();

			JSONObject partitionJSON = new JSONObject(new JSONTokener(partition.toJson()));
			JSONArray clusters = partitionJSON.getJSONArray("conceptsOfNeighbours");

			String targetId = partitionJSON.getJSONArray("target").getString(0)
					.replaceAll("^.*#", "")
					.replaceAll("_.*$", "");

			int nbClusters = clusters.length();

			for(int i = 0; i < nbClusters; i++) {
				JSONObject cluster = clusters.getJSONObject(i);
				int id = cluster.getInt("id");
				int extensionalDistance = cluster.getInt("extensionalDistance");

				Set<String> compatibleRelations = model.getCompatibleRelations(model.getTestPair(targetId));

				int totalCompatibleExamples = 0;
				for(String relation : compatibleRelations) {
					totalCompatibleExamples += model.getRelationTypeCounter(relation);
				}

				JSONArray answers = cluster.getJSONArray("answers");
				Map<String, Integer> countByRelation = new HashMap<>();

				for(int j = 0; j < answers.length(); j++) {
					JSONArray ans = answers.getJSONArray(j);
					String pairId = ans.getString(0).replaceAll(".*#", "").replaceAll("_.*", "");
					String relation = model.getTrainingPair(pairId).getRelation();
					if(countByRelation.containsKey(relation)) {
						countByRelation.put(relation, countByRelation.get(relation) + 1);
					} else {
						countByRelation.put(relation, 1);
					}
				}

				int finalTotalCompatibleExamples = totalCompatibleExamples;
				countByRelation.forEach((rel, count) -> {
					if(!scores.containsKey(rel)) {
						scores.put(rel, new ArrayList<>());
					}
					PrivateScore score = new PrivateScore();
					score.id = id;
					double confidence = ((double) countByRelation.get(rel)) / (extensionalDistance + LAMBDA);
					double prio = (double) model.getRelationTypeCounter(rel) / finalTotalCompatibleExamples;
					double rectifiedConfidence;
					if(prio > confidence) {
						rectifiedConfidence = 0;
					} else {
						rectifiedConfidence = (confidence - prio) / (1 - prio);

					}

					score.score = rectifiedConfidence;
					scores.get(rel).add(score);
				});

				for(String rel : scores.keySet()) {
					scores.get(rel).sort((s1, s2) -> -Double.compare(s1.score, s2.score));
				}
			}

			return scores;
		}

		@Override
		public JSONArray score(RelationExtractionModel model, Partition partition) {
			Map<String, List<PrivateScore>> scores = this.scoring(model, partition);

			List<String> sortedRel = this.sort(scores);

			JSONArray jsonScores = new JSONArray();

			for(String rel : sortedRel) {
				JSONObject score = new JSONObject();
				score.put("relation", rel);
				JSONArray detailedScores = new JSONArray();
				for(PrivateScore privateScore : scores.get(rel)) {
					JSONObject detailedScore = new JSONObject();
					detailedScore.put("id", privateScore.id);
					detailedScore.put("score", privateScore.score);
					detailedScores.put(detailedScore);
				}
				score.put("score", detailedScores);
				jsonScores.put(score);
			}

			return jsonScores;
		}

		@Override
		public String toString() {
			return "maximum_rectified_confidence";
		}

		private List<String> sort(Map<String, List<PrivateScore>> scores) {
			List<String> sorted = new ArrayList<>(scores.keySet());

			sorted.sort((rel1, rel2) -> -this.comparatorAux(scores.get(rel1), scores.get(rel2), 0));

			return sorted;
		}

		private int comparatorAux(List<PrivateScore> scores1, List<PrivateScore> scores2, int index) {
			if(scores1.size() <= index && scores2.size() <= index) {
				return 0;
			} else if(scores1.size() <= index) {
				return 1;
			} else if(scores2.size() <= index) {
				return -1;
			} else if(scores1.get(index).score == scores2.get(index).score) {
				return comparatorAux(scores1, scores2, index + 1);
			} else {
				return (scores1.get(index).score < scores2.get(index).score ? -1 : 1);
			}

		}

		private static class PrivateScore {
			public int id;
			public double score;
		}
	}
}
