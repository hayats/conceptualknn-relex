package conceptualKNN_RelEx.scorer;

import conceptualKNN_RelEx.utils.RelExException;

public class ScorerRegistry {
	public final static Scorer EXPONENTIAL_VOTE = new Scorer.ExponentialVote();
	public final static Scorer MAXIMUM_CONFIDENCE = new Scorer.MaximumConfidence();
	public final static Scorer MAXIMUM_RECTIFIED_CONFIDENCE = new Scorer.MaximumRectifiedConfidence();
	public final static Scorer MAXIMUM_CONFIDENCE_BIS = new Scorer.MaximumConfidenceBis();

	public static Scorer getScorer(String name) throws RelExException {
		switch(name) {
			case "maximum_confidence":
				return MAXIMUM_CONFIDENCE;
			case "exponential_vote":
				return EXPONENTIAL_VOTE;
			case "maximum_rectified_confidence":
				return MAXIMUM_RECTIFIED_CONFIDENCE;
			case "maximum_confidence_bis" :
				return MAXIMUM_CONFIDENCE_BIS;
			default:
				throw new RelExException("Trying to get a nonexistent scorer");
		}
	}
}
