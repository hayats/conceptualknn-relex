package conceptualKNN_RelEx;

import conceptualKNN_RelEx.experiments.BaselineRunner;
import conceptualKNN_RelEx.experiments.Experiment;
import conceptualKNN_RelEx.tacredCoreNLP.TacredCoreNLPProcessor;
import conceptualKNN_RelEx.utils.*;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import net.sourceforge.argparse4j.inf.Subparsers;

import java.io.IOException;

public class Main {
	public static void main(String[] args) throws RelExException, IOException, InterruptedException {
		ArgumentParser argParser = ArgumentParsers.newFor("C-KNN-RelEx").build();

		Subparsers subparsers = argParser.addSubparsers()
				.dest("command")
				.title("Commands")
				.metavar("COMMAND")
				.help("Choose which command to execute");

		Experiment.addCliArgs(subparsers.addParser("experiment").help(Experiment.DESCRIPTION));
		AnalyzeResults.addCliArgs(subparsers.addParser("analyze-results").help(AnalyzeResults.DESCRIPTION));
		TacredCoreNLPProcessor.addCliArgs(subparsers.addParser("corenlp-processor").help(TacredCoreNLPProcessor.DESCRIPTION));
		BaselineRunner.addCliArgs(subparsers.addParser("baseline-runner").help(BaselineRunner.DESCRIPTION));
		LukeToTacred.addCliArgs(subparsers.addParser("luke-to-tacred").help(LukeToTacred.DESCRIPTION));
		AnalyzeLuke.addCliArgs(subparsers.addParser("analyze-luke").help(AnalyzeLuke.DESCRIPTION));
		CompareApproaches.addCliArgs(subparsers.addParser("compare-approaches").help(CompareApproaches.DESCRIPTION));


		try {
			Namespace namespace = argParser.parseArgs(args);
			switch(namespace.getString("command")) {
				case "experiment":
					Experiment experiment = new Experiment(namespace);
					experiment.execute();
					break;
				case "analyze-results":
					AnalyzeResults.execute(namespace);
					break;
				case "corenlp-processor":
					TacredCoreNLPProcessor.execute(namespace);
					break;
				case "baseline-runner":
					BaselineRunner.execute(namespace);
					break;
				case "luke-to-tacred":
					LukeToTacred.execute(namespace);
					break;
				case "analyze-luke":
					AnalyzeLuke.execute(namespace);
					break;
				case "compare-approaches":
					CompareApproaches.execute(namespace);
			}
		} catch(ArgumentParserException e) {
			argParser.handleError(e);
		}
	}
}
