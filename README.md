# ConceptualKNN-RelEx

Extracting relations with FCA-based Concepts of Neighbours.
Preliminary results are presented in [this paper](https://link.springer.com/chapter/10.1007/978-3-030-77867-5_10).

## Installation

### Prerequisites

* **Wordnet 3.1 dictionaries**: [download link](http://wordnetcode.princeton.edu/wn3.1.dict.tar.gz)
* **TACRED**: the dataset is not freely available, [link to the dataset website](https://nlp.stanford.edu/projects/tacred/#intro) 
* **JAVA 11**: installation depending of your system

### Download

The most recent version of the program is accessible in the latest release.

## Usage

### Reprocessing TACRED with CoreNLP

Example using *v21.08.03*:

```bash
$ java -jar ConceptualKNN-RelEx.jar corenlp-processor \
    <path to tacred> <path to output dir>
   ```

To display full help:

```bash
$ java -jar ConceptualKNN-RelEx.jar corenlp-processor -h
```

### Merge the reprocessed dataset and the output of LUKE-ReDect

LUKE-ReDect can be found [here](https://gitlab.inria.fr/hayats/luke-redect)

```bash
$ java -jar ConceptualKNN-RelEx.jar luke-to-tacred -v \
  /path/to/tacred/test/file \
  /path/to/luke/test/output/file \
  /path/to/output/file
```

### Reproducing results

The full help can be displayed with:

```bash
$ java -jar ConceptualKNN-RelEx.jar experiment -h
```

For example, to reproduce the experiment with a ten-second timeout and a pruned dependency tree on a four-core machine:

```bash
$ java -jar ConceptualKNN-RelEx.jar experiment \
    -v -n 4 -t 10 \
    /path/to/reprocessed/tacred/directory \
    /path/to/wordnet/dictionary \
    /path/to/output/dir
```

### Run the baseline

To run the baseline and display the results:
```bash
$ java -jar ConceptualKNN-RelEx.jar baseline-runner \
    -v \
    /path/to/TACRED/dataset \
    /path/to/output/directory
```

The baseline produces a gold file and a prediction file.

Full help for the baseline can be displayed with:
```bash
$ java -jar ConceptualKNN-RelEx.jar baseline-runner -h
```